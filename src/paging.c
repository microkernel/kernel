#include <printk.h>
#include <string.h>
#include <kmalloc.h>
#include "interrupt.h"
#include "paging.h"
#include "frames.h"
#include "mem.h"
#include "malloc.h"

extern uint32_t pg_dir;
extern uint32_t pg0;
extern uint32_t pg1;
extern uint32_t end;
page_directory_t *page_directory;
uint32_t current_virtual_address;

void *get_virtual_address(void* addr)
{
    return (ptr_t)((uint32_t) addr) + KERNEL_VIRTUAL_ADDRESS;
}

void memset_paging(void *ptr)
{
    for (uint32_t i = 0; i < 1024; i++) {
        ((uint32_t *) ptr)[i] = 0;
    }
}

void set_page_directory(page_directory_t *dir)
{
    load_page_directory(dir);
}

void init_page_table(page_t *page_table, uint32_t start)
{
    uint32_t address;

    address = start * PAGE_SIZE;
    for (uint32_t i = start; i < 1024; i++) {
        ((uint32_t *) page_table)[i] = address | 3;
        address += PAGE_SIZE;
    }
}

int8_t get_page_from_virtual_address(page_t **page, uint32_t addr)
{
	uint32_t pg_dir = ADDRESS_TO_PAGEDIR_INDEX(addr);
	uint32_t pg_tab = ADDRESS_TO_PAGETAB_INDEX(addr);

	if (page_directory[pg_dir].present)
	{
		page_table_t *table = (ptr_t)FRAME_TO_ADDRESS((addr_t)(page_directory[pg_dir].frame));
		*page = table->pages + pg_tab;
		return 0;
	}
	return -1;	
}

int8_t map_page_at_virtual_address(page_t **page, addr_t vaddr, addr_t paddr)
{
  const uint32_t pg_dir = ADDRESS_TO_PAGEDIR_INDEX(vaddr);
  const uint32_t pg_tab = ADDRESS_TO_PAGETAB_INDEX(vaddr);

  if (!paddr)
    return unmap_page_at_virtual_address(vaddr);

  if (page_directory[pg_dir].present)
  {
    page_table_t *table = (ptr_t)FRAME_TO_ADDRESS(page_directory[pg_dir].frame);
    *page = table->pages + pg_tab;
    (*page)->frame = ADDRESS_TO_FRAME(paddr);
    (*page)->present = 1;
    (*page)->rw = 1;
    (*page)->user = 0;
    (*page)->accessed = 0;
    (*page)->dirty = 0;

    invlpg(vaddr);
    return 0;
  }
  else
  {
    // create that fucker
    page_directory[pg_dir].present = 1;
    page_directory[pg_dir].rw = 1;

    uint32_t page_addr = current_virtual_address;
    page_t *ptr = 0;
    get_page_from_virtual_address(&ptr, page_addr);
    page_directory[pg_dir].frame = ptr->frame;
    current_virtual_address += PAGE_SIZE;

    memset_paging((ptr_t)page_addr);

    // setup that sucker
    page_table_t *table = (page_table_t *)(page_addr);
    *page = table->pages + pg_tab;
    (*page)->frame = ADDRESS_TO_FRAME(paddr);
    (*page)->present = 1;
    (*page)->rw = 1;
    (*page)->user = 0;
    (*page)->accessed = 0;
    (*page)->dirty = 0;

    invlpg(vaddr);

    return 1;
  }
  return -1;
}

int8_t remap_page_at_virtual_address(addr_t orig_vaddr, addr_t dest_vaddr)
{
  page_t *page = 0;

  get_page_from_virtual_address(&page, orig_vaddr);

  if (!page)
    return -1;

  return map_page_at_virtual_address(&page, dest_vaddr, FRAME_TO_ADDRESS(page->frame));
}

int8_t movemap_page_at_virtual_address(addr_t orig_vaddr, addr_t dest_vaddr)
{
  page_t *page = 0;

  get_page_from_virtual_address(&page, orig_vaddr);

  if (!page)
    return -1;

  if (map_page_at_virtual_address(&page, dest_vaddr, FRAME_TO_ADDRESS(page->frame)) == -1)
    return -1;
  unmap_page_at_virtual_address(orig_vaddr);
  return 0;
}

int8_t unmap_page_at_virtual_address(addr_t vaddr)
{
  uint32_t pg_dir = ADDRESS_TO_PAGEDIR_INDEX(vaddr);
  uint32_t pg_tab = ADDRESS_TO_PAGETAB_INDEX(vaddr);

  if (page_directory[pg_dir].present)
  {
    page_table_t *table = (ptr_t)FRAME_TO_ADDRESS(page_directory[pg_dir].frame);
    page_t *page = table->pages + pg_tab;

    if (!page->present)
      return 0;

    page->present = 0;
    page->frame = 0;
    page->rw = 0;
    page->user = 0;
    page->accessed = 0;
    page->dirty = 0;
    invlpg(vaddr);
    return 1;
  }
  return 0;
}

uint32_t get_first_aligned_page(uint32_t address)
{
    if (address & 0xFFFFF000)
    {
        address &= 0xFFFFF000;
        address += 0x1000;
    }
    return address;
}

void initial_mapping(uint32_t page_directory, uint32_t page_table)
{
    uint32_t index = 0;
    uint32_t address = 0;
    page_directory_t *pg = (ptr_t)page_directory;

    pg->present = 1;
    pg->rw = 1;
    pg->frame = ADDRESS_TO_FRAME((uint32_t) page_table);

    while (index < 400)
    {

        ((uint32_t *) page_table)[index++] = address | 3;
        address += PAGE_SIZE;
    }
    while (index < 1024)
        ((uint32_t *) page_table)[index++] = 0;
}

void map_kernel(page_directory_t *page_directory,
                page_table_t *page_table)
{
    uint32_t index = 0;
    uint32_t address = 0;
    page_directory_t *pg = &((page_directory)[ADDRESS_TO_PAGEDIR_INDEX(KERNEL_VIRTUAL_ADDRESS)]);

    pg->present = 1;
    pg->rw = 1;
    pg->frame = ADDRESS_TO_FRAME((uint32_t) page_table);

    while (index < 400)
    {
        ((uint32_t *) page_table)[index++] = address | 3;
        address += PAGE_SIZE;
    }
    while (index < 1024)
        ((uint32_t *) page_table)[index++] = 0;
}

void initialise_paging()
{
    page_directory = (ptr_t)&pg_dir;

  memset(&pg_dir, 0, PAGE_SIZE);

  initial_mapping((addr_t)&pg_dir, (addr_t)&pg0);

  // map the kernel
  map_kernel((ptr_t)&pg_dir, (ptr_t)&pg1);

  set_page_directory((ptr_t)&pg_dir);
  _enable_paging();

  // unmap the kernel
//   kunmap_area_soft((ptr_t)KERNEL_PHYSICAL_ADDRESS + 4096 * 15, 1);

  page_directory = get_virtual_address(&pg_dir);

  current_virtual_address = align_address((ptr_t)get_virtual_address(&end));
}
