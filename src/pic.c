
#include "types.h"
#include "asm_inoutb.h"
#include "irq.h"

// command codes
#define PIC_READ_IRR                0x0a    /* OCW3 irq ready next CMD read */
#define PIC_READ_ISR                0x0b    /* OCW3 irq service next CMD read */

// Initialization codes
#define ICW1_ICW4 0x01    /* ICW4 (not) needed */
#define ICW1_SINGLE 0x02    /* Single (cascade) mode */
#define ICW1_INTERVAL4  0x04    /* Call address interval 4 (8) */
#define ICW1_LEVEL  0x08    /* Level triggered (edge) mode */
#define ICW1_INIT 0x10    /* Initialization - required! */

#define ICW4_8086 0x01    /* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO 0x02    /* Auto (normal) EOI */
#define ICW4_BUF_SLAVE  0x08    /* Buffered mode/slave */
#define ICW4_BUF_MASTER 0x0C    /* Buffered mode/master */
#define ICW4_SFNM 0x10    /* Special fully nested (not) */


void pic_remap(uint8_t offset_master, uint8_t offset_slave)
{
  unsigned char mask_1, mask_2;

  mask_1 = inb(PIC1_DATA);        // save masks
  mask_2 = inb(PIC2_DATA);

  outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4); // starts the initialization sequence (in cascade mode)
  dummy_delay();
  outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
  dummy_delay();
  outb(PIC1_DATA, offset_master); // ICW2: Master PIC vector offset
  dummy_delay();
  outb(PIC2_DATA, offset_slave);  // ICW2: Slave PIC vector offset
  dummy_delay();
  outb(PIC1_DATA, 4);             // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
  dummy_delay();
  outb(PIC2_DATA, 2);             // ICW3: tell Slave PIC its cascade identity (0000 0010)
  dummy_delay();

  outb(PIC1_DATA, ICW4_8086);
  dummy_delay();
  outb(PIC2_DATA, ICW4_8086);
  dummy_delay();

  outb(PIC1_DATA, mask_1);        // restore saved masks.
  outb(PIC2_DATA, mask_2);
}

// helper, read master and slave pics, concatenate results in the return value
static uint16_t _pic_get_irq_reg(int ocw3)
{
    outb(PIC1_COMMAND, ocw3);
    outb(PIC2_COMMAND, ocw3);
    return (inb(PIC2_COMMAND) << 8) | inb(PIC1_COMMAND);
}

// return the combined IRR
uint16_t pic_get_irr(void)
{
    return _pic_get_irq_reg(PIC_READ_IRR);
}

// return the combined ISR
uint16_t pic_get_isr(void)
{
    return _pic_get_irq_reg(PIC_READ_ISR);
}

////////////////////////////////////////////////////////////////////////////////
// IRQ things
////////////////////////////////////////////////////////////////////////////////

void irq_deactivate(uint8_t line)
{
  uint16_t port = PIC1_DATA;

  if (line >= 8)
  {
    port = PIC2_DATA;
    line -= 8;
  }
  outb(port, inb(port) | (1 << line));
}

void irq_activate(uint8_t line)
{
  uint16_t port = PIC1_DATA;

  if (line >= 8)
  {
    port = PIC2_DATA;
    line -= 8;
  }
  outb(port, inb(port) & ~(1 << line));
}

