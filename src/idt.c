
#include "asm_interrupt.h"
#include "interrupt.h"
#include <kernel.h>
#include <serial.h>

struct idt_entry
{
  uint16_t offset_1;
  uint16_t cs;
  uint8_t zero;
  uint8_t type: 5; // gate type: 0x5: task, 0xe: interrupt, 0xf: call
  uint8_t dpl: 2;  // RING [0..3]
  uint8_t present: 1;
  uint16_t offset_2;
} __attribute__((packed));

struct idt_entry idt[256];
interrupt_handler_t redirect_table[256] = {0}; // forward control over the function here with some infos as parameter


void idt_set_gate(uint8_t number, uint8_t privilege, interrupt_handler_t callback)
{
  redirect_table[number] = callback;
  idt[number].dpl = privilege;
  idt[number].present = (callback != 0);
}

// from the assembly code
void __idt_asm_init();
void pic_remap(uint8_t offset_master, uint8_t offset_slave); /// \brief initialize the PIC


void page_fault_handler(struct interrupt_parameters *p)
{
  printk("\n ** %Minterrupt %u%D: (with code %X).\n%M"
         "\tEIP: %X\tFLAGS: %b\t\n"
         "\tEAX: %X\tEBX: %X\tECX: %X\n"
         "\tEDX: %X\tEDI: %X\tESI: %X\n"
         "\tESP: %X\tEBP: %X\n"
         "%D", KCC_TO_MODE(KCC_BLACK, KCC_RED), p->interrupt_number,
         p->error_code, KCC_TO_MODE(KCC_BLACK, KCC_LIGHT_RED), p->eip, p->eflags, p->eax, p->ebx, p->ecx, p->edx, p->edi, p->esi, p->ebp, p->esp);

  printk_console(&serial_console, "\n%M## addr2line -e kernel.bin %x%D\n", KCC_TO_MODE(KCC_BLACK, KCC_LIGHT_CYAN), p->eip);
  printk("PAGE_FAULT: %x\n", p->eip);
  bug();
}

void _idt_install()
{
  int i = 0;

  INTERRUPT_CRITICAL_SECTION;

  // call the assembly function that fills redirect_table with the handlers
  __idt_asm_init();

  // re-initialize redirect_table and the idt table
  for (; i < 256; ++i)
  {
    addr_t ptr = (addr_t)redirect_table[i];

    idt[i].offset_1 = ptr & 0xFFFF;
    idt[i].cs = 0x08;
    idt[i].zero = 0;
    idt[i].type = IDT_INTERUPT_GATE_TYPE;
    idt[i].dpl = 0;
    idt[i].present = 0;
    idt[i].offset_2 = (ptr >> 16) & 0xFFFF;

    redirect_table[i] = 0;
  }

  _idt_setup_default_routines();
   idt_set_gate(14, 0, page_fault_handler);
 
  // setup idtr
  set_idt(idt, sizeof(idt) - 1);

  // setup the pic
  pic_remap(IRQ_INTERRUPT_START_NUMBER, IRQ_INTERRUPT_START_NUMBER + 0x8);

  INTERRUPT_CRITICAL_SECTION_END;
}

