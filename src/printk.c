#include "printk.h"
#include <string.h>
#include <stdargs.h>
#include <kernel.h>

/* describe the vga console */
static struct console *kernel_console = &vga_console;

void console_clear()
{
  console_clear_console(kernel_console);
}

void console_clear_console(struct console *cons)
{
  cons->cursor_x = 0;
  cons->cursor_y = 0;
  cons->clear(cons);
}

static void do_putchar(struct console *console, char c)
{
  if (console->handle_special_chars)
  {
    console->putchar(console, c);
  }
  else // no special chars for this console !
  {
    switch (c)
    {
      case '\n':
      {
        if (console->cursor_y + 1 < console->size_y)
          console->cursor_y += 1;
        else
          console->scroll_buffer(console, 1); /* scroll one line */

        console->cursor_x = 0;
      }
      break;

      case '\r':
        console->cursor_x = 0; /* \r does not clear ! */
        break;

      case '\t':
      {
        int to_inc = 4 - (console->cursor_x % 4);
        while (to_inc--)
          do_putchar(console, ' ');
      }
      break;

      case '\v': /* vertical tab */
      {
        if (console->cursor_y < console->size_y)
          console->cursor_y += 1;
        else
          console->scroll_buffer(console, 1); /* scroll one line */
      }
      break;

      case '\b':
      {
        if (console->cursor_x > 0)
          --console->cursor_x; /* \b does not clear ! */
      }
      break;

      default:
      {
        console->putchar(console, c);
        if (c)
        {
          if (console->cursor_x + 1 < console->size_x)
            console->cursor_x++;
          else
            do_putchar(console, '\n'); /* next line */
        }
      }
    }
  }
}

static size_t ipow_base(size_t number, size_t base_len)
{
  size_t i = 1;

  for (; number >= base_len; i *= base_len)
    number /= base_len;
  return i;
}

static size_t print_unumber(size_t number, const char *base, int force_sign, struct console *cons)
{
  const size_t base_len = strlen(base);
  const size_t pow = ipow_base(number, base_len);

  if (force_sign)
    do_putchar(cons, '+');

  if (!number)
    do_putchar(cons, base[0]);
  for (size_t i = pow; number && i; i /= base_len)
  {
    const size_t tn = number / (i);
    do_putchar(cons, base[tn % base_len]);
  }

  return (force_sign ? 0 : 1);
}

static size_t print_number(long number, const char *base, int force_sign, struct console *cons)
{
  size_t count = 0;

  if (number < 0)
  {
    do_putchar(cons, '-');
    number *= -1;
    ++count;
  }
  else if (force_sign)
  {
    do_putchar(cons, '+');
    ++count;
  }
  return count + print_unumber(number, base, 0, cons);
}

void printk_str_console(struct console *cons, const char *s)
{
  while (*s)
    do_putchar(cons, *s++);
  do_putchar(cons, '\0');
}

void printk_str(const char *s)
{
  printk_str_console(kernel_console, s);
}

void va_printk_console(struct console *cons, const char *fmt, va_list lst)
{
  while (*fmt)
  {
    if (*fmt == '%')
    {
      ++fmt;
      switch (*fmt)
      {
        case '%':
        {
          do_putchar(cons, '%');
          break;
        }
        case 'x':
        {
          size_t number = va_arg(lst, unsigned int);
          printk_str_console(cons, "0x");
          print_unumber(number, BASE_16_DIGITS_LOWER, 0, cons);
          break;
        }
        case 'X':
        {
          size_t number = va_arg(lst, unsigned int);
          printk_str_console(cons, "0x");
          print_unumber(number, BASE_16_DIGITS, 0, cons);
          break;
        }
        case 'o':
        {
          size_t number = va_arg(lst, unsigned int);
          do_putchar(cons, '0');
          print_unumber(number, BASE_8_DIGITS, 0, cons);
          break;
        }
        case 'b':
        {
          size_t number = va_arg(lst, unsigned int);
          do_putchar(cons, 'b');
          print_unumber(number, "01", 0, cons);
          break;
        }
        case 'U':
        case 'u':
        {
          size_t number = va_arg(lst, unsigned int);
          print_unumber(number, BASE_10_DIGITS, 0, cons);
          break;
        }
        case 'i':
        {
          long number = va_arg(lst, int);
          print_number(number, BASE_10_DIGITS, 0, cons);
          break;
        }
        case 's':
        {
          const char *str = va_arg(lst, const char *);
          printk_str_console(cons, str);
          break;
        }
        case 'c':
        {
          char c = va_arg(lst, int);
          do_putchar(cons, c);
          break;
        }
        case 'M':
        {
          uint32_t mode = va_arg(lst, unsigned int);
          cons->mode = mode;
          break;
        }
        case 'D':
        {
          cons->mode = DEFAULT_MODE;
          break;
        }
        default:
          continue;
      };
      ++fmt;
      continue;
    }
    putchar_console(cons, *fmt++);
  }
  do_putchar(cons, '\0'); /* move the cursor */
}

void printk_console(struct console *cons, const char *fmt, ...)
{
  va_list lst;

  va_start(lst, fmt);
  va_printk_console(cons, fmt, lst);
  va_end(lst);
}

void va_printk(const char *fmt, va_list lst)
{
  va_printk_console(kernel_console, fmt, lst);
}

void printk(const char *fmt, ...)
{
  va_list lst;

  va_start(lst, fmt);
  va_printk_console(kernel_console, fmt, lst);
  va_end(lst);
}

void putchar_console(struct console *cons, char c)
{
  do_putchar(cons, c);
  do_putchar(cons, '\0'); /* move the cursor */
}

void putchar(char c)
{
  putchar_console(kernel_console, c);
}

struct console *get_kernel_console()
{
  return kernel_console;
}

void set_kernel_console(struct console *cons)
{
  if (cons != kernel_console)
  {
    kernel_console = cons;
    kernel_console->cursor_x = 0;
    kernel_console->cursor_y = 0;
    kernel_console->init(kernel_console);
//     kernel_console->clear(kernel_console);
    do_putchar(kernel_console, '\0'); /* move the cursor */
  }
}

void _init_printk()
{
  kernel_console->cursor_x = 0;
  kernel_console->cursor_y = 0;
  kernel_console->init(kernel_console);
  kernel_console->clear(kernel_console);
  do_putchar(kernel_console, '\0'); /* move the cursor */
}

