#include "kernel.h"
#include "kmalloc.h"
#include "mem.h"
#include "paging.h"
#include "scheduler.h"

task_t	*first_task = 0;
extern uint32_t current_virtual_address;
extern page_directory_t *page_directory;

uint32_t get_physical_page_directory(page_directory_t *new_page_directory)
{
  page_t *ptr = 0;
  get_page_from_virtual_address(&ptr, (uint32_t)new_page_directory);
  return FRAME_TO_ADDRESS(ptr->frame);
}

void switch_task ( task_t *task )
{
	uint32_t tmp = get_physical_page_directory((ptr_t)task->tss.cr3);

	// 	 "mov %3, %%eax;
	//      "mov %%eax, %%cr3;       
    __asm__ volatile ( "\
     mov %0, %%ecx;       \
     mov %1, %%esp;       \
     mov %2, %%ebp;       \
     mov $0x12345, %%eax; \
     sti;                 \
     jmp *%%ecx           "
     : : "r" ( task->tss.eip ), "r" ( task->tss.esp ), "r" ( task->tss.ebp ), "r" (tmp) );
}

void init()
{
	while (1) {
    for (volatile int i = 0; i < 400000; ++i);
		printk ("Hello world!\n");
	}
}

void create_task ( task_t *task )
{
    task->priority = 1000;
    task->tss.eip = (addr_t)&init;
    task->tss.ebp = (addr_t)kmap_linear ( 10 );
    task->tss.esp = task->tss.ebp;
    task->tss.cr3 = (addr_t)kmap_linear ( 1 );
    map_kernel ( (ptr_t)task->tss.cr3, (ptr_t)kmap_linear ( 1 ) );
	
}

void init_scheduler()
{
    first_task = kmap_linear(1);
    memset ( first_task, 0, PAGE_SIZE );

    create_task ( first_task );
    switch_task ( first_task );
}
