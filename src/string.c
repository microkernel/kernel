
#include <string.h>


/*
** Get the length of a C string
*/
size_t strlen(const char *s)
{
  size_t len = 0;

  if (!s)
    return (0);
  while (s[len])
    ++len;
  return (len);
}

size_t strnlen(const char *s, size_t maxlen)
{
  size_t len = 0;

  if (!s)
    return (0);
  while (len < maxlen && s[len])
    ++len;
  return (len);
}

/*
** Get the length of a C string, until a character or \0
*/
size_t strlen_delim(const char *s, char delim)
{
  size_t len = 0;

  if (!s)
    return (0);
  while ((s[len]) && (s[len] != delim))
    ++len;
  return (len);
}

/*
** Get the length of a C string, until a character of delim or \0
*/
size_t strlen_delims(const char *s, const char *delims)
{
  size_t i;
  size_t j;

  i = 0;
  while (s && s[i])
  {
    j = 0;
    while (delims && delims[j])
    {
      if (s[i] == delims[j])
        return (i);
      ++j;
    }
    ++i;
  }
  return (i);
}

/*
** copy a s2 in s1
** s1 must have the good size
*/
char  *strcpy(char *s1, const char *s2)
{
  size_t i = 0;

  if (!s1 || !s2)
    return (0);
  while (s2[i])
  {
    s1[i] = s2[i];
    ++i;
  }
  s1[i] = '\0';
  return ((char *)(s1));
}

/*
** copy at most n char of s2 in s1
** s1 must have the good size
*/
char  *strncpy(char *s1, const char *s2, size_t n)
{
  size_t i = 0;

  if (!s1 || !s2)
    return (0);
  while ((s2[i]) && (i < n))
  {
    s1[i] = s2[i];
    ++i;
  }
  s1[i] = '\0';
  return ((char *)(s1));
}

/*
** compare s1 and s2
*/
int strcmp(const char *s1, const char *s2)
{
  size_t i = 0;

  if (!s1 || !s2)
    return (strlen(s1) - strlen(s2));
  while (s1[i] && s2[i] && (s1[i] == s2[i]))
    ++i;
  return (s1[i] - s2[i]);
}

/*
** compare at most n char of s1 and s2
*/
int strncmp(const char *s1, const char *s2, size_t n)
{
  size_t i = 0;

  if (!s1 || !s2)
    return (strlen(s1) - strlen(s2));
  while (s1[i] && s2[i] && (s1[i] == s2[i]) && (i < n))
    ++i;
  return (s1[i] - s2[i]);
}

/*
** concatenate the string: append s2 to s1
** s1 must have the correct size
*/
char  *strcat(char *s1, const char *s2)
{
  size_t i = 0;
  size_t p;

  if (!s1 || !s2)
    return (s1);
  p = strlen(s1);
  while (s2[i])
    s1[p++] = s2[i++];
  s1[p] = '\0';
  return ((char *)s1);
}

/*
** concatenate the string, append at most n char to s1
** s1 must have the correct size
*/
char  *strncat(char *s1, const char *s2, size_t n)
{
  size_t i = 0;
  size_t p;

  if (!s1 || !s2)
    return (s1);
  p = strlen(s1);
  while ((s2[i]) && (i < n))
    s1[p++] = s2[i++];
  s1[p] = '\0';
  return ((char *)s1);
}


/*
** string to int conversion
*/
static int   extract_sign(const char *str, size_t *new_pos)
{
  int   sign = 1;
  size_t pos = 0;

  while ((*(str + pos) != 0) &&
   ((*(str + pos) == '+') || (*(str + pos) == '-')))
  {
    if (*(str + pos) == '-')
      sign *= -1;
    ++pos;
  }
  *new_pos = pos;
  return (sign);
}

int   isdigit_base(char c, const char *base)
{
  size_t i = 0;

  while ((base[i] != '\0') && (c != '\0'))
  {
    if (base[i] == c)
      return (i);
    ++i;
  }
  return (-1);
}

size_t atoul_base(const char *str, const char *base)
{
  size_t number = 0;
  int   digit;
  size_t pos = 0;
  size_t base_num = strlen(base);

  digit = isdigit_base(str[pos], base);
  while ((digit != -1) && (base_num > 1))
  {
    number = number * base_num + digit;
    ++pos;
    digit = isdigit_base(str[pos], base);
  }
  return (number);
}

long atol_base(const char *str, const char *base)
{
  size_t advance = 0;
  int sign;

  sign = extract_sign(str, &advance);
  return (long)(atoul_base(str + advance, base)) * sign;
}

// number to string
static size_t ipow_base(size_t number, size_t base_len)
{
  size_t i = 1;

  for (; number >= base_len; i *= base_len)
    number /= base_len;
  return i;
}

size_t ultoa_base(size_t number, const char *base, char *str, size_t maxsize)
{
  const size_t base_len = strlen(base);
  const size_t pow = ipow_base(number, base_len);
  size_t count = 0;

  if (!maxsize)
    return 0;
  if (!number)
    str[count++] = (base[0]);
  for (size_t i = pow; number && i && count < maxsize; i /= base_len)
  {
    const size_t tn = number / (i);
    str[count++] = (base[tn % base_len]);
  }

  return count;
}

size_t ltoa_base(long number, const char *base, char *str, size_t maxsize)
{
  size_t count = 0;

  if (!maxsize)
    return 0;

  if (number < 0)
  {
    str[count] = '-';
    number *= -1;
    ++count;
  }
  return count + ultoa_base(number, base, str + count, maxsize - count);
}