#include "mem.h"
#include "types.h"
#include <gdt.h>

tss_t			tss = {0};
uint64_t		*gdt;
struct gdt_t	*gdt_ptr;

static void gdt_set_gate ( gdt_entry_t *gdt,
                           unsigned long base,
                           unsigned long limit,
                           uint8_t access,
                           uint8_t gran )
{
    /* Setup the descriptor base access */
    gdt->base_low = ( base & 0xFFFF );
    gdt->base_middle = ( base >> 16 ) & 0xFF;
    gdt->base_high = ( base >> 24 ) & 0xFF;

    /* Setup the descriptor limits */
    gdt->limit_low = ( limit & 0xFFFF );
    gdt->granularity = ( ( limit >> 16 ) & 0x0F );

    /* Finally, set up the granularity and access flags */
    gdt->granularity |= ( gran & 0xF0 );
    gdt->access = access;
}

void enable_paging()
{
    gdt_t gdtr;
    gdtr.base = ( uint32_t ) gdt;
    gdtr.limit = sizeof ( gdt ) - 1;

    __asm__ volatile (
        "lgdt %0\n"
        : /* no output */
        : "m" ( gdtr )
        : "memory" );
}

void init_gdt ( void *stack_ptr )
{
    tss.ss0 = 0x10;
    tss.esp0 = ( uint32_t ) stack_ptr;
    gdt = &_gdt;
    /* Setup the GDT pointer and limit */
    memset ( gdt, 0, 4096 );

    // kernel
    gdt_set_gate ( (void *)&gdt[1], 0, 0xFFFFFFFF, 0x9A, 0xCF );
    gdt_set_gate ( (void *)&gdt[2], 0, 0xFFFFFFFF, 0x92, 0xCF );

    // usermode
    gdt_set_gate ( (void *)&gdt[3], 0, 0xFFFFFFFF, 0xFA, 0xCF );
    gdt_set_gate ( (void *)&gdt[4], 0, 0xFFFFFFFF, 0xF2, 0xCF );
    //Set the TSS
    gdt_set_gate ( (void *)&gdt[5], (addr_t)&tss, sizeof ( tss ), TSS_TYPE, 0x2 );

    gdt_ptr = ( gdt_t * ) &_gdt_ptr;
    gdt_ptr->limit = ( sizeof ( *gdt ) * 6 ) - 1;
    gdt_ptr->base = ( uint32_t ) gdt;
    gdt_flush ( ( uint32_t ) gdt_ptr );
    uint16_t	tr = 5 * 8;
    __asm__ volatile ( "ltr %0" : : "r" ( tr ) );
}
