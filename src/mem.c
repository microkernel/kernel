
#include "kernel.h"

void *memcpy(unsigned char *dest, const void *src, int count)
{
  int i;
  for (i = 0; i < count; i++)((uint8_t *)dest)[i] = ((uint8_t *)src)[i];
  return dest;
}

void *memset(void *dest, uint8_t val, int count)
{
  int i;
  for (i = 0; i < count; i++)((uint8_t *)dest)[i] = val;
  return dest;
}
