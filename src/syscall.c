
#include <kernel.h>
#include <interrupt.h>
#include <syscall.h>
#include <errno.h>

static syscall_handler syscall_table[SYSCALL_TABLE_SIZE];

static int syscall_default_hander(struct interrupt_parameters *p, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
  printk("%Munknow syscall: %i %M (possible params: %x, %x, %x, %x, %x)%D\n", KCC_TO_MODE(KCC_RED, KCC_WHITE), p->eax,
         KCC_TO_MODE(KCC_BLACK, KCC_CYAN), a1, a2, a3, a4, a5);

  return -ENOSYS;
}

static void _sys_int_handler(struct interrupt_parameters *p)
{
  uint32_t sysno = p->eax;
  int ret = -1;

  if (syscall_table[sysno])
    ret = syscall_table[sysno](p, p->ebx, p->ecx, p->edx, p->esi, p->edi);
  else
    ret = syscall_default_hander(p, p->ebx, p->ecx, p->edx, p->esi, p->edi);

  p->eax = ret;
}

void _init_syscalls()
{
  // reset the table
  for (uint32_t i = 0; i < SYSCALL_TABLE_SIZE; ++i)
    syscall_table[i] = 0;

  // register the handler
  idt_set_gate(SYSCALL_INTERRUPT, 3, _sys_int_handler);
}

void syscall_set_handler(uint32_t sysno, syscall_handler syshandler)
{
  PANIC_IF(sysno >= SYSCALL_TABLE_SIZE, "registering a syscall: sysno is too big.");

  syscall_table[sysno] = syshandler;
}

