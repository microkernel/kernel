#include "kernel.h"
#include "asm_inoutb.h"
#include <paging.h>
#include <gdt.h>
#include <interrupt.h>
#include <paging.h>
#include <serial.h>
#include <syscall.h>
#include <elf.h>
#include <keyboard.h>
#include <kmalloc.h>

static void handle_PIT_ticks(struct interrupt_parameters *p)
{
  static uint32_t counter = 0;

  IRQ_INTERRUPT_PROLOG(p);
  ++counter;

  printk_console(&serial_console, "\r%M[tick: %u]         %D\r", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), counter);
}

uint32_t base_ptr;
static int SYS_print(struct interrupt_parameters *p, char *str, uint32_t size)
{
  (void)p;
  uint32_t i = 0;

  // as str is mapped (or should be !) in the process memory space, it needs to be displaced to it
  str = base_ptr + str;
  printk("%M[SYSCALL-PRINT]: %D", KCC_TO_MODE(KCC_BLACK, KCC_YELLOW));
  while (i < size)
  {
    putchar(str[i]);
    ++i;
  }
  return i;
}

static int SYS_panic(struct interrupt_parameters *p)
{
  (void)p;
  // this could be used with addr2line: addr2line -e build/elf/prog/main_1.elf.bin [EIP]
  printk("%M[SYSCALL-PANIC]:%M program panic (EIP is: %x)%D\n", KCC_TO_MODE(KCC_BLACK, KCC_YELLOW), KCC_TO_MODE(KCC_BLACK, KCC_RED), p->eip - base_ptr);
  printk_console(&serial_console, "%M[SYSCALL-PANIC]:%M program panic: addr2line -e build/elf/prog/main_1.elf.bin %x%D\n", KCC_TO_MODE(KCC_BLACK, KCC_YELLOW), KCC_TO_MODE(KCC_BLACK, KCC_RED), p->eip - base_ptr);
  return 0;
}

#define KBD_DATA_PORT           0x60

static void handle_keyboard(struct interrupt_parameters *p)
{
  IRQ_INTERRUPT_PROLOG(p);

  static int shift_key = 0;

  // get the keyboard key stroke:
  uint8_t keycode = inb(KBD_DATA_PORT);

  uint16_t charcode = kbd_get_char_code(keycode, shift_key);
  if ((charcode & KMK_RELEASED) && (charcode & KMK_SHIFT))
    shift_key = 0;
  else if (!(charcode & KMK_RELEASED) && (charcode & KMK_SHIFT))
    shift_key = 1;

  // print the key
  if (charcode == '\b')
    printk_console(&vga_console, "%M\b \b%D", KCC_TO_MODE(KCC_BLACK, KCC_DARK_GRAY), charcode & 0xFF);
  if ((charcode & 0xFF) > 8 && (charcode & 0xFF) < 127 && !(charcode & KMK_RELEASED))
    printk_console(&vga_console, "%M%c%D", KCC_TO_MODE(KCC_BLACK, KCC_DARK_GRAY), charcode & 0xFF);
  /* if ((charcode & 0xFF) > 20 && (charcode & 0xFF) < 127)
      printk_console(&serial_console, "%M[keyboard: %c %s]%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), charcode & 0xFF, charcode & KMK_RELEASED ? "released" : "");
    else
      printk_console(&serial_console, "%M[keyboard: %x %s]%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), charcode & 0xFF, charcode & KMK_RELEASED ? "released" : "");*/
}

extern int _binary_build__elf____prog_main_1_elf_start;
extern uint32_t current_virtual_address;
static inline void _kinit()
{
  _disable_interrupts();

  _idt_install();

  irq_set_handler(0, handle_PIT_ticks);
  irq_set_handler(1, handle_keyboard);

  _enable_interrupts();

  _init_syscalls();

  // set the serial console as the default kernel console
  set_kernel_console(&serial_console);

  // syscalls
  syscall_set_handler(5, (syscall_handler)SYS_print);
  syscall_set_handler(254, (syscall_handler)SYS_panic);

  // load a test programm.
  uint32_t st_addr = KERNEL_VIRTUAL_ADDRESS_END + 4096;
  base_ptr = st_addr + 4096;
  if (!kmap_area_to((void *) st_addr, 5))
    PANIC("out of mem !");

  void *ptr = (void *) load_elf(&_binary_build__elf____prog_main_1_elf_start, 4096, (void *) st_addr);

  void (*fptr)() = ptr + st_addr;
  // call the start function, that will return right after
  set_kernel_console(&vga_console);
  fptr();
  set_kernel_console(&serial_console);


  // force the output to the VGA console
  printk_console(&vga_console, "[everything from the kernel is now written in the %Mserial%D console]\n", KCC_TO_MODE(KCC_BLACK, KCC_WHITE));
  printk_console(&vga_console, "\t%M-> look at your terminal ;)%D\n", KCC_TO_MODE(KCC_BLACK, KCC_WHITE));
}

int _main(unsigned long magic, addr_t stack_ptr)
{
  init_gdt((void *)stack_ptr);
  initialise_paging();

  _init_printk();

  // Store the stack position in a global pointer
  struct multiboot_info *mbi = (struct multiboot_info *) stack_ptr;

  PANIC_IF(magic != MULTIBOOT_BOOTLOADER_MAGIC, "Wrong magic number");
  PANIC_IF(mbi->mods_count > 0, "Multiboot got overwritten");
  _kinit();

  printk("%s: %Mkernel started !%D\n", __DATE__, KCC_TO_MODE(KCC_BLACK, KCC_WHITE));

//   init_scheduler();

  while (1);
  return (0);
}
