
#include "printk.h"
#include "asm_inoutb.h"

#define DEFAULT_COLOR   0x07
#define ROWS        25
#define COLUMNS     126
#define VGA_START   0xB8000

static volatile short *vga_buffer = (volatile short *)(VGA_START);

static void vga_putchar(struct console *self, char c)
{
  if (c)
    vga_buffer[self->cursor_x + self->cursor_y * self->size_x] = ((self->mode & 0xFF) << 8) | c;
  else /* \0: move the cursor */
  {
    const uint16_t cursor = self->cursor_x + self->cursor_y * self->size_x;
    outb(0x3D4, 14);
    outb(0x3D5, cursor >> 8);
    outb(0x3D4, 15);
    outb(0x3D5, cursor);
  }
}

static void vga_clear(struct console *self)
{
  int i = 0;
  const int max = self->size_x * self->size_y;

  while (i < max)
    vga_buffer[i++] = ((self->mode & 0xFF) << 8) | ' ';
}

/* software scroll (instead of the int10 one) */
static void vga_scroll(struct console *self, unsigned short line)
{
  int ry = line;
  int wy = 0;

  /* simple tests */
  if (line == 0)
    return;
  if (line >= self->size_y)
    return vga_clear(self);

  /* copy the lower part of the buffer upper */
  for (; ry < self->size_y; ++ry, ++wy)
  {
    int x = 0;

    /* copy a line */
    for (; x < self->size_x; ++x)
      vga_buffer[x + wy * self->size_x] = vga_buffer[x + ry * self->size_x];
  }

  /* clear the 'new' lower part */
  wy = self->size_y - line;
  for (; wy < self->size_y; ++wy)
  {
    int x = 0;

    /* clear a line */
    for (; x < self->size_x; ++x)
      vga_buffer[x + wy * self->size_x] = ((self->mode & 0xFF) << 8) | ' ';
  }
}

#if 0
unsigned char g_90x60_text[] =
{
/* MISC */
  0xF1,
/* SEQ */
  0x03, 0x00, 0x03, 0x00, 0x06,
/* CRTC */
  0x6B, COLUMNS - 1/*-*/, 0x5A, 0x82, 0x60, 0x8D, 0x0B, 0x3C,/*|*/
  0x00, 0x4F, 0x06, 0x07, 0x00, 0x00, 0x00, 0x00,
  0xEA, 0x0C, 0xDF/*|*/, ((COLUMNS) / 2)/*|*/, 0x08, 0xE8, 0x05, 0xA3,
  0xFF,
};

#define VGA_AC_INDEX    0x3C0
#define VGA_AC_READ     0x3C1
#define VGA_MISC_WRITE  0x3C2
#define VGA_SEQ_INDEX   0x3C4
#define VGA_SEQ_DATA    0x3C5

#define VGA_CRTC_INDEX  0x3D4   /* 0x3B4 */
#define VGA_CRTC_DATA   0x3D5   /* 0x3B5 */
#define VGA_INSTAT_READ 0x3DA

#define VGA_NUM_SEQ_REGS  5
#define VGA_NUM_CRTC_REGS 25

void write_regs(unsigned char *regs)
{
  unsigned i;

/* write MISCELLANEOUS reg */
  outb(VGA_MISC_WRITE, *regs);
  regs++;
/* write SEQUENCER regs */
  for(i = 0; i < VGA_NUM_SEQ_REGS; i++)
  {
    outb(VGA_SEQ_INDEX, i);
    outb(VGA_SEQ_DATA, *regs);
    regs++;
  }
/* unlock CRTC registers */
  outb(VGA_CRTC_INDEX, 0x03);
  outb(VGA_CRTC_DATA, inb(VGA_CRTC_DATA) | 0x80);
  outb(VGA_CRTC_INDEX, 0x11);
  outb(VGA_CRTC_DATA, inb(VGA_CRTC_DATA) & ~0x80);
/* make sure they remain unlocked */
  regs[0x03] |= 0x80;
  regs[0x11] &= ~0x80;
/* write CRTC regs */
  for(i = 0; i < VGA_NUM_CRTC_REGS; i++)
  {
    outb(VGA_CRTC_INDEX, i);
    outb(VGA_CRTC_DATA, *regs);
    regs++;
  }
/* lock 16-color palette and unblank display */
  inb(VGA_INSTAT_READ);
  outb(VGA_AC_INDEX, 0x20);
}*/
#endif
static void vga_init(struct console *self)
{
#if 0
  write_regs(g_90x60_text);

  self->cursor_x = 0;
  self->cursor_y = 0;
  self->size_x = COLUMNS;
  self->size_y = 30;
  self->mode = DEFAULT_COLOR;
#endif
  self->cursor_x = 0;
  self->cursor_y = 0;
  self->size_x = 80;
  self->size_y = 25;
  self->mode = DEFAULT_COLOR;
}

struct console vga_console = {0, 0, COLUMNS, ROWS, DEFAULT_COLOR, 0, vga_putchar, vga_clear, vga_scroll, vga_init};
