#include "paging.h"
#include "interrupt.h"

# define INDEX_FROM_BIT(a) (a/(8*4))
# define OFFSET_FROM_BIT(a) (a%(8*4))


page_directory_t *kernel_directory = 0;
page_directory_t *current_directory = 0;

extern uint32_t *frames;
extern uint32_t nframes;

extern uint32_t placement_address;

void initialise_paging()
{
    uint32_t mem_end_page = 0x1000000;

    nframes = mem_end_page / 0x1000;
    frames = (uint32_t *) kmalloc(INDEX_FROM_BIT(nframes));
    memset(frames, 0, INDEX_FROM_BIT(nframes));

    // Let's make a page directory.
    kernel_directory = (page_directory_t *) kmalloc_a(sizeof(page_directory_t));
    memset(kernel_directory, 0, sizeof(*kernel_directory));
    current_directory = kernel_directory;

    // We need to identity map (phys addr = virt addr) from
    // 0x0 to the end of used memory, so we can access this
    // transparently, as if paging wasn't enabled.
    // NOTE that we use a while loop here deliberately.
    // inside the loop body we actually change placement_address
    // by calling kmalloc(). A while loop causes this to be
    // computed on-the-fly rather than once at the start.
    int i = 0;
    while (i < placement_address) {
        // Kernel code is readable but not writeable from userspace.
        alloc_frame(get_page(i, 1, kernel_directory), 0, 0);
        i += 0x1000;
    }
    // Before we enable paging, we must register our page fault handler.
    idt_set_gate(14, 0, page_fault_handler);
//    register_interrupt_handler(14, page_fault);

    // Now, enable paging!
    switch_page_directory(kernel_directory);
}

void page_fault_handler()
{
    printk("Coucou\n");
}

void switch_page_directory(page_directory_t *dir)
{
    current_directory = dir;
    asm volatile(
    "mov %0, %%cr3"
    :
    :"r" (&dir->physical_tables)
    );
//    uint32_t cr0;
//    asm volatile("mov %%cr0, %0": "=r"(cr0));
//    cr0 |= 0x80000000; // Enable paging!
//    asm volatile("mov %0, %%cr0"::"r"(cr0));
}

page_t *get_page(uint32_t address, int make, page_directory_t *dir)
{
    // Turn the address into an index.
    address /= 0x1000;
    // Find the page table containing this address.
    uint32_t table_idx = address / 1024;
    if (dir->tables[table_idx]) // If this table is already assigned
    {
        return &dir->tables[table_idx]->pages[address % 1024];
    }
    else if (make) {
        uint32_t tmp;
        dir->tables[table_idx] = (page_table_t *) kmalloc_ap(sizeof(page_table_t), &tmp);
        memset(dir->tables[table_idx], 0, 0x1000);
        dir->physical_tables[table_idx] = tmp | 0x7; // PRESENT, RW, US.
        return &dir->tables[table_idx]->pages[address % 1024];
    }
    else {
        return 0;
    }
}