
#include "kmalloc.h"
#include "paging.h"
#include "frames.h"
#include "kernel.h"

void *kmap_area(size_t page_count)
{
  (void)page_count;
  printk("%MKMEM/kmap_area:%M NOT IMPLEMENTED !%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
  return 0;
}

void *kmap_area_to(void *vaddr, size_t page_count)
{
  vaddr = (void *)align_address(vaddr);

  for (size_t i = 0; i < page_count; ++i)
  {
    uint32_t frame = frame_get_first_free();
    page_t *ptr = 0;

    if ((((addr_t)vaddr + 4096 * i) >= KERNEL_VIRTUAL_ADDRESS) && (((addr_t)vaddr + 4096 * i) <= KERNEL_VIRTUAL_ADDRESS_END))
    {
      printk("%MKMEM/kmap_area_to:%M WANTING TO OVERWRITE KERNEL !%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      if (i)
        kunmap_area(vaddr, i - 1);
      return 0;
    }

    if (!frame)
    {
      printk("%MKMEM/kmap_area_to:%M NO MORE MEMORY (allocation size: %i pages)%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED), page_count);
      if (i)
        kunmap_area(vaddr, i - 1);
      return 0;
    }

    map_page_at_virtual_address(&ptr, (addr_t)vaddr + 4096 * i, frame);
  }

  return vaddr;
}

void kunmap_area(void *vaddr, size_t page_count)
{
  vaddr = (void *)align_address(vaddr);
  page_t *ptr = 0;

  for (size_t i = 0; i < page_count; ++i)
  {
    uint32_t frame = 0;
    ptr = 0;
    if ((((addr_t)vaddr + 4096 * i) >= KERNEL_VIRTUAL_ADDRESS) && (((addr_t)vaddr + 4096 * i) <= KERNEL_VIRTUAL_ADDRESS_END))
    {
      printk("%MKMEM/kunmap_area:%M WANTING TO OVERWRITE KERNEL !%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      return;
    }

    if (get_page_from_virtual_address(&ptr, (addr_t)vaddr + 4096 * i) == -1)
    {
      printk("%MKMEM/kunmap_area:%M could not get page data%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      return;
    }

    if (!ptr)
      continue;

    frame = FRAME_TO_ADDRESS(ptr->frame);
    if (unmap_page_at_virtual_address((addr_t)vaddr + 4096 * i) == -1)
    {
      printk("%MKMEM/kunmap_area:%M could not unmap page%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      return;
    }
    frame_push_to_free_list(frame);
  }
}

void kunmap_area_soft(void *vaddr, size_t page_count)
{
  vaddr = (void *)align_address(vaddr);

  for (size_t i = 0; i < page_count; ++i)
  {
    if ((((addr_t)vaddr + 4096 * i) >= KERNEL_VIRTUAL_ADDRESS) && (((addr_t)vaddr + 4096 * i) <= KERNEL_VIRTUAL_ADDRESS_END))
    {
      printk("%MKMEM/kunmap_area:%M WANTING TO OVERWRITE KERNEL !%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      return;
    }
    if (unmap_page_at_virtual_address((addr_t)vaddr + 4096 * i) == -1)
    {
      printk("%MKMEM/kunmap_area_soft:%M could not unmap page%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      return;
    }
  }
}

extern uint32_t current_virtual_address;

void *kmap_linear(size_t page_count)
{
  page_t *page = 0;

  if (!page_count)
    return 0;
  get_page_from_virtual_address(&page, current_virtual_address);
  void *st_page = (void *)current_virtual_address;
  if (!page || !page->present) // map that page
  {
    if (kmap_area_to(st_page, page_count) == 0)
      return 0;
  }

  current_virtual_address += PAGE_SIZE * page_count;
  return st_page;
}

