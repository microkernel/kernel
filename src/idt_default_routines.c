
#include <kernel.h>
#include <interrupt.h>
#include <serial.h>

#define INT_PANIC(msg)  PANIC("** interrupt: " msg);

static void __intr_noop(struct interrupt_parameters *p);
static void __intr_double_fault(struct interrupt_parameters *p) __attribute__((unused));
static void __intr_divide_by_zero(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("DIVISION BY ZERO");}
static void __intr_single_step(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("SINGLE STEP");}
static void __intr_NMI(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("NMI -^");}
static void __intr_breakpoint(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("BREAKPOINT");}
static void __intr_overflow(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("OVERFLOW");}
static void __intr_bounds_check(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("BOUNDS CHECK");}
static void __intr_illegal_opcode(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("ILLEGAL OPCODE :D");}
static void __intr_device_not_available(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("DEVICE NOT AVAILABLE");}
static void __intr_double_fault(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("DOUBLE FAULT :(");}
static void __intr_coprocessor_seg(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("COPROCESSOR SEGMENT OVERRUN");}
static void __intr_invalid_task_segment(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("INVALID TASK SEGMENT :)");}
static void __intr_segment_not_present(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("SEGMENT NOT PRESENT");}
static void __intr_stack_fault_exception(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("STACK FAULT EXCEPTION");}
static void __intr_segfault(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("the true story is that a segfault is always here, hidden,\n     waiting for its time :)");}
static void __intr_page_fault(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("PAGE FAULT (yep, you died for that)");}
static void __intr_unassigned(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("UNASSIGNED");}
static void __intr_FPU(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("FPU ERROR");}
static void __intr_alignment_check(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("ALIGNMENT CHECK");}
static void __intr_machine_check(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("MACHINE CHECK");}
static void __intr_SIMD_EXCEPTION(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("SIMD EXCEPTION");}
static void __intr_reserved(struct interrupt_parameters *p){__intr_noop(p);INT_PANIC("[reserved exception] (20-31)");}
static void __intr_IRQ(struct interrupt_parameters *p){  IRQ_INTERRUPT_PROLOG(p); printk("\n%M ** IRQ: %u%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), p->interrupt_number - IRQ_INTERRUPT_START_NUMBER);}
static void __intr_kernel_defined(struct interrupt_parameters *p){__intr_noop(p);/*INT_PANIC("[kernel defined interrupt] (32-255)");*/}

static void __intr_noop(struct interrupt_parameters *p)
{
  printk("\n ** %Minterrupt %u%D: (with code %X).\n%M"
         "\tEIP: %X\tFLAGS: %b\t\n"
         "\tEAX: %X\tEBX: %X\tECX: %X\n"
         "\tEDX: %X\tEDI: %X\tESI: %X\n"
         "\tESP: %X\tEBP: %X\n"
         "%D", KCC_TO_MODE(KCC_BLACK, KCC_RED), p->interrupt_number,
         p->error_code, KCC_TO_MODE(KCC_BLACK, KCC_LIGHT_RED), p->eip, p->eflags, p->eax, p->ebx, p->ecx, p->edx, p->edi, p->esi, p->ebp, p->esp);

  printk_console(&serial_console, "\n%M## addr2line -e kernel.bin %x%D\n", KCC_TO_MODE(KCC_BLACK, KCC_LIGHT_CYAN), p->eip);
}

void _idt_setup_default_routines()
{
  int i;

  idt_set_gate(0, 0, __intr_divide_by_zero);
  idt_set_gate(1, 0, __intr_single_step);
  idt_set_gate(2, 0, __intr_NMI);
  idt_set_gate(3, 0, __intr_breakpoint);
  idt_set_gate(4, 0, __intr_overflow);
  idt_set_gate(5, 0, __intr_bounds_check);
  idt_set_gate(6, 0, __intr_illegal_opcode);
  idt_set_gate(7, 0, __intr_device_not_available);
  idt_set_gate(8, 0, __intr_double_fault);
  idt_set_gate(9, 0, __intr_coprocessor_seg);
  idt_set_gate(10, 0, __intr_invalid_task_segment);
  idt_set_gate(11, 0, __intr_segment_not_present);
  idt_set_gate(12, 0, __intr_stack_fault_exception);
  idt_set_gate(13, 0, __intr_segfault);
  idt_set_gate(14, 0, __intr_page_fault);
  idt_set_gate(15, 0, __intr_unassigned);
  idt_set_gate(16, 0, __intr_FPU);
  idt_set_gate(17, 0, __intr_alignment_check);
  idt_set_gate(18, 0, __intr_machine_check);
  idt_set_gate(19, 0, __intr_SIMD_EXCEPTION);

  for (i = 20; i < 31; ++i)
    idt_set_gate(i, 0, __intr_reserved);

  for (;i < 255; ++i)
    idt_set_gate(i, 0, __intr_kernel_defined);

  for (i = IRQ_INTERRUPT_START_NUMBER; i < IRQ_INTERRUPT_END_NUMBER; ++i)
    idt_set_gate(i, 0, __intr_IRQ);
}

