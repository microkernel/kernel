#include "kernel.h"
#include "paging.h"
// #include "kmalloc.h"
#include "mem.h"
#include "frames.h"

extern uint32_t		end;

struct frame_t
{
  struct frame_t *next;
};

#define LFP_ADDR  MEM_START_PAGE

uint32_t next_free_frame = 0;
ptr_t first_free_frame = (ptr_t)LFP_ADDR;

uint32_t last_used_frame = (addr_t)&end;

uint32_t frame_get_first_free()
{
  if (last_used_frame == 0)
    last_used_frame = (addr_t)align_address((void *)&end);

  uint32_t frame = 0;

  // no more free page in the list
  // get the next one
  if (next_free_frame == 0 /*&& last_used_frame < 0xFFFF0000*/)
  {
    frame = last_used_frame;
    last_used_frame += 4096;
  }
  else if (next_free_frame != 0)
  {
    // we have something in the free list
    frame = next_free_frame;

    // swap with the next
    addr_t *n = (addr_t *)(first_free_frame);
    next_free_frame = *n;

    // map the next page (unmap the current)
    page_t *p = 0;
    map_page_at_virtual_address(&p, LFP_ADDR, (addr_t)n);
  }

  return frame;
}

void frame_push_to_free_list(uint32_t frame)
{
  // the easy case
  if (frame + 4096 == last_used_frame)
  {
    last_used_frame -= 4096;
    return;
  }

  // save the next free frame
  uint32_t next_frame = next_free_frame;

  // map the next page (unmap the current)
  page_t *p = 0;
  map_page_at_virtual_address(&p, LFP_ADDR, frame);

  addr_t *n = (addr_t *)(first_free_frame);
  *n = next_frame;
}


uint32_t	align_address(void *ptr)
{
	uint32_t address = (uint32_t) ptr;

	if (address & 0xFFFFF000)
    {
        address &= 0xFFFFF000;
        address += 0x1000;
    }
    return address;
}
