
#include <elf.h>
#include <libelf.h>
#include <printk.h>
#include <mem.h>
#include <frames.h>

// check the magic number of the ELF file
static inline int elf_check_image_magic(Elf32_Ehdr *hdr)
{
//   printk("%MELF-LOADER:%M header: %x%c%c%c%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_YELLOW), hdr->e_ident[EI_MAG0],
//          hdr->e_ident[EI_MAG1],
//          hdr->e_ident[EI_MAG2],
//          hdr->e_ident[EI_MAG3]);
  return hdr->e_ident[EI_MAG0] == ELFMAG0
      && hdr->e_ident[EI_MAG1] == ELFMAG1
      && hdr->e_ident[EI_MAG2] == ELFMAG2
      && hdr->e_ident[EI_MAG3] == ELFMAG3
      && hdr->e_version == EV_CURRENT;
}

static inline int elf_check_image_class(Elf32_Ehdr *hdr)
{
  return hdr->e_ident[EI_CLASS] == ELFCLASS32 && hdr->e_ident[EI_DATA] == ELFDATA2LSB && hdr->e_machine == EM_386;
}

static inline int elf_check_image_type(Elf32_Ehdr *hdr)
{
  return (hdr->e_type == ET_EXEC) || (hdr->e_type == ET_DYN);
}

uint32_t load_elf(void *start_elf_addr, uint32_t elf_size, void *base_addr)
{
  (void)elf_size; // TODO: this may corrupt memory !

  Elf32_Ehdr *hdr = start_elf_addr;
  Elf32_Phdr *phdr = 0;

  uint32_t disp = 0; // could be used to implement something that look like ASLR

  printk("%MELF-LOADER:%D loading a new ELF image @%X...\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), start_elf_addr);
  if (!elf_check_image_magic(hdr))
  {
    printk("%MELF-LOADER:%M Unknow file type%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
    return 0;
  }
  if (!elf_check_image_class(hdr))
  {
    printk("%MELF-LOADER:%M bad file class (not a 32 bit i386 elf image)%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
    return 0;
  }
  if (!elf_check_image_type(hdr))
  {
    printk("%MELF-LOADER:%M bad file type (not an executable one)%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
    return 0;
  }

  disp += 4096;

  // get the prog. header
  phdr = (Elf32_Phdr *)(start_elf_addr + hdr->e_phoff);

  for (int i = 0; i < hdr->e_phnum; ++i)
  {
    // skip a section that we don't need to load
    if (phdr[i].p_type != PT_LOAD || !phdr[i].p_filesz)
      continue;

    // hu... what should we keep in that case ?
    if (phdr[i].p_filesz > phdr[i].p_memsz)
    {
      printk("%MELF-LOADER:%M p_filesz > p_memsz%D\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), KCC_TO_MODE(KCC_BLACK, KCC_RED));
      return 0;
    }

    // p_filesz can be smaller than p_memsz,
    // the difference is zeroe'd out.
    void *start = start_elf_addr + phdr[i].p_offset;
    void *daddr = phdr[i].p_vaddr + base_addr + disp;

    memcpy(daddr, start, phdr[i].p_filesz);

    // zeroes the remaining of the section
    memset(daddr + phdr[i].p_filesz, 0x0, phdr[i].p_memsz - phdr[i].p_filesz);

    if (!(phdr[i].p_flags & PF_W))
    {
      // TODO: read-only.
    }

    if (phdr[i].p_flags & PF_X)
    {
      // TODO: executable.
    }
  }

  printk("%MELF-LOADER:%D loaded ELF image @%X with entry point @%X\n", KCC_TO_MODE(KCC_BLACK, KCC_CYAN), start_elf_addr, hdr->e_entry + disp);

  return hdr->e_entry + disp;
}

