
#include <keyboard.h>
#include <paging.h>
#include "keymap.h"

uint16_t kbd_get_char_code(uint8_t stroke, int shift_key)
{
  uint16_t nfo = 0;
  if (stroke >= 0x80)
  {
    stroke -= 0x80;
    nfo |= KMK_RELEASED;
  }

  if (shift_key)
    shift_key = 1;

  return ((uint16_t *)get_virtual_address(us_keymap))[KEYMAP_COLS * stroke + shift_key] | nfo;
}

