
#include <serial.h>
#include <string.h>

// init:
void serial_init()
{
  outb(PORT + 1, 0x00);    // Disable all interrupts
  outb(PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
  outb(PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
  outb(PORT + 1, 0x00);    //                  (hi byte)
  outb(PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
  outb(PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
  outb(PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set
}

// the console stuff:

static uint16_t old_mode = 0xFFFF;

// KCC is index, serial (terminfo) is value
static char *kcc_to_serial_modes = "0426153774261537";

#define S_MOVE_CUR_SEQ  "\e[Y;XH"
#define S_MODE_SEQ  "\e[BFm"
#define S_DEF_MODE_SEQ  "\e[m"
static void serial_putchar(struct console *self, char c)
{
  // output the mode
  if (self->mode != old_mode)
  {
    char tarray[11];
    tarray[0] = '\e';
    tarray[1] = '[';
    tarray[2] = '4';
    tarray[3] = kcc_to_serial_modes[((self->mode >> 4) & 0x0F)];
    tarray[4] = 'm';
    tarray[5] = '\e';
    tarray[6] = '[';
    tarray[7] = '3';
    tarray[8] = kcc_to_serial_modes[((self->mode & 0x0F))];
    tarray[9] = 'm';
    tarray[10] = '\0';
    serial_out_cstr(tarray);
    old_mode = self->mode;
  }

  if (c)
    serial_outb(c);
//   else /* \0: move the cursor */
//   {
// //     char tarray[35] = {0};
// //     size_t ret = 2;
// //     tarray[0] = '\e';
// //     tarray[1] = '[';
// //     ret += ultoa(self->cursor_y + 1, tarray + ret, 15 - ret);
// //     tarray[ret++] = ';';
// //     ret += ultoa(self->cursor_x + 1, tarray + ret, 30 - ret);
// //     tarray[ret++] = 'H';
// // 
// //     serial_out_cstr(tarray);
//   }
}

#define S_CLEAR_SEQ     "\e[H\e[2J"
static void serial_clear(struct console *self)
{
  (void)self;

  serial_out_cstr(S_CLEAR_SEQ);
}

static void serial_scroll(struct console *self, unsigned short line)
{
  (void)self;
  (void)line;

  serial_outb('\n');
}

static void console_serial_init(struct console *self)
{
  (void)self;
  serial_init();
}

struct console serial_console = {0, 0, 80, 25, DEFAULT_MODE, 1, serial_putchar, serial_clear, serial_scroll, console_serial_init};

