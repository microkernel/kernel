//
// file : keymap.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/keymap.h
//
// created by : Timothée Feuillet on linux.site
// date: 20/02/2015 20:43:54
//

#ifndef __N_13974412381746335530_75662114__KEYMAP_H__
# define __N_13974412381746335530_75662114__KEYMAP_H__

#include "types.h"
#include <keyboard.h>

// from http://www.julienlecomte.net/simplix/
#define SCAN_CODES_COUNT 128
#define KEYMAP_COLS       2


static uint16_t us_keymap[SCAN_CODES_COUNT * KEYMAP_COLS] =
{
/*==========================================
    scancode       !shift           shift
  ==========================================*/
    /*  0 */              0,                  0,
    /*  1 */         KMK_ESCAPE,     KMK_SHIFT + KMK_ESCAPE,
    /*  2 */            '1',                '!',
    /*  3 */            '2',                '@',
    /*  4 */            '3',                '#',
    /*  5 */            '4',                '$',
    /*  6 */            '5',                '%',
    /*  7 */            '6',                '^',
    /*  8 */            '7',                '&',
    /*  9 */            '8',                '*',
    /* 10 */            '9',                '(',
    /* 11 */            '0',                ')',
    /* 12 */            '-',                '_',
    /* 13 */            '=',                '+',
    /* 14 */           '\b',       KMK_SHIFT + '\b',
    /* 15 */           '\t',       KMK_SHIFT + '\t',
    /* 16 */            'q',                'Q',
    /* 17 */            'w',                'W',
    /* 18 */            'e',                'E',
    /* 19 */            'r',                'R',
    /* 20 */            't',                'T',
    /* 21 */            'y',                'Y',
    /* 22 */            'u',                'U',
    /* 23 */            'i',                'I',
    /* 24 */            'o',                'O',
    /* 25 */            'p',                'P',
    /* 26 */            '[',                '{',
    /* 27 */            ']',                '}',
    /* 28 */           '\n',       KMK_SHIFT + '\n',
    /* 29 */           KMK_CTRL,       KMK_SHIFT + KMK_CTRL,
    /* 30 */            'a',                'A',
    /* 31 */            's',                'S',
    /* 32 */            'd',                'D',
    /* 33 */            'f',                'F',
    /* 34 */            'g',                'G',
    /* 35 */            'h',                'H',
    /* 36 */            'j',                'J',
    /* 37 */            'k',                'K',
    /* 38 */            'l',                'L',
    /* 39 */            ';',                ':',
    /* 40 */           '\'',                '"',
    /* 41 */            '`',                '~',
    /* 42 */         KMK_SHIFT,     KMK_SHIFT,
    /* 43 */           '\\',                '|',
    /* 44 */            'z',                'Z',
    /* 45 */            'x',                'X',
    /* 46 */            'c',                'C',
    /* 47 */            'v',                'V',
    /* 48 */            'b',                'B',
    /* 49 */            'n',                'N',
    /* 50 */            'm',                'M',
    /* 51 */            ',',                '<',
    /* 52 */            '.',                '>',
    /* 53 */            '/',                '?',
    /* 54 */         KMK_SHIFT,     KMK_SHIFT,
    /* 55 */            '*',                '*',
    /* 56 */            KMK_ALT,        KMK_SHIFT + KMK_ALT,
    /* 57 */            ' ',                ' ',
    /* 58 */      KMK_CAPS_LOCK,  KMK_SHIFT + KMK_CAPS_LOCK,
    /* 59 */             KMK_F1,        KMK_SHIFT +  KMK_F1,
    /* 60 */             KMK_F2,        KMK_SHIFT +  KMK_F2,
    /* 61 */             KMK_F3,        KMK_SHIFT +  KMK_F3,
    /* 62 */             KMK_F4,        KMK_SHIFT +  KMK_F4,
    /* 63 */             KMK_F5,        KMK_SHIFT +  KMK_F5,
    /* 64 */             KMK_F6,        KMK_SHIFT +  KMK_F6,
    /* 65 */             KMK_F7,        KMK_SHIFT +  KMK_F7,
    /* 66 */             KMK_F8,        KMK_SHIFT +  KMK_F8,
    /* 67 */             KMK_F9,        KMK_SHIFT +  KMK_F9,
    /* 68 */            KMK_F10,        KMK_SHIFT + KMK_F10,
    /* 69 */       KMK_NUM_LOCK,   KMK_SHIFT + KMK_NUM_LOCK,
    /* 70 */       KMK_SCR_LOCK,   KMK_SHIFT + KMK_SCR_LOCK,
    /* 71 */           KMK_HOME,       KMK_SHIFT + KMK_HOME,
    /* 72 */             KMK_UP,         KMK_SHIFT + KMK_UP,
    /* 73 */           KMK_PGUP,       KMK_SHIFT + KMK_PGUP,
    /* 74 */            '-',                '-',
    /* 75 */           KMK_LEFT,       KMK_SHIFT + KMK_LEFT,
    /* 76 */         KMK_CENTER,     KMK_SHIFT + KMK_CENTER,
    /* 77 */          KMK_RIGHT,      KMK_SHIFT + KMK_RIGHT,
    /* 78 */            '+',                '+',
    /* 79 */            KMK_END,        KMK_SHIFT + KMK_END,
    /* 80 */           KMK_DOWN,       KMK_SHIFT + KMK_DOWN,
    /* 81 */           KMK_PGDN,       KMK_SHIFT + KMK_PGDN,
    /* 82 */         KMK_INSERT,     KMK_SHIFT + KMK_INSERT,
    /* 83 */         KMK_DELETE,     KMK_SHIFT + KMK_DELETE,
    /* 84 */              0,                  0,
    /* 85 */              0,                  0,
    /* 86 */              0,                  0,
    /* 87 */            KMK_F11,        KMK_SHIFT + KMK_F11,
    /* 88 */            KMK_F12,        KMK_SHIFT + KMK_F12,
    /* 89 */             0,                   0,
    /* All other keys are undefined */
};

#endif /*__N_13974412381746335530_75662114__KEYMAP_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

