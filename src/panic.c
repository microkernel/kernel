
#include "kernel.h"
#include <asm_interrupt.h>
#include <asm_inoutb.h>

// almost like panic, but without parameters
void bug()
{
  PANIC("KERNEL BUG :) (keep smiling !)");
}

void panic(char *str)
{
  _disable_interrupts();

  // print the panic text
  printk("\n%M--- PANIC ! ---%D\n\n", KCC_TO_MODE(KCC_RED, KCC_WHITE));
  printk("%Mkernel build on: "__DATE__", "__TIME__"\n", KCC_TO_MODE(KCC_BLACK, KCC_RED));
  printk("kernel panic: ");
  printk("%M%s", KCC_TO_MODE(KCC_BLACK, KCC_WHITE), str);
  printk("\n\n%M--- ------- ---%D\n\n", KCC_TO_MODE(KCC_RED, KCC_WHITE));

  if (get_kernel_console() != &vga_console)
    printk_console(&vga_console, "\n\n%M-- [KERNEL PANIC] --%D\n\n", KCC_TO_MODE(KCC_RED, KCC_WHITE));

  // wait.
  while (1)
  {
    for (size_t i = 0; i < 0xFFFFFF; ++i)
      dummy_delay();
    printk_str("\b+");
    for (size_t i = 0; i < 0xFFFFFF; ++i)
      dummy_delay();
    printk_str("\b ");
  }
}
