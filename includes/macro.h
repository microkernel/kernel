//
// file : macro.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/macro.h
//
// created by : Timothée Feuillet on linux.site
// date: 13/01/2015 00:29:01
//

#ifndef __N_14529729081722304335_2033458861__MACRO_H__
# define __N_14529729081722304335_2033458861__MACRO_H__

#define STRINGIFY(s) SUBSTRINGIFY(s)
#define SUBSTRINGIFY(s) #s

#endif /*__N_14529729081722304335_2033458861__MACRO_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

