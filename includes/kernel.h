/* 
 * Includes for MicroKernel
 */
#include "macro.h"
#include "printk.h"
#include "multiboot.h"


// printk str and panic
void panic(char *str);

// some usefull macros
#define PANIC(message)   panic(__FILE__": " STRINGIFY(__LINE__) ":\n  " message)
#define PANIC_IF(cond, message)   do{if (cond) PANIC("condition '" STRINGIFY(cond) "':\n  " message);}while(0)

// almost like panic, but without parameters
void bug();
