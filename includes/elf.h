//
// file : elf.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/elf.h
//
// created by : Timothée Feuillet on linux.site
// date: 19/02/2015 09:08:56
//

#ifndef __N_6721276161852313001_268001708__ELF_H__
# define __N_6721276161852313001_268001708__ELF_H__

#include <types.h>

/// \brief load a binary at a given base address
/// \note base_addr should be the start of the program address space, mapped to the kernel space
///        (i.e. the 0x000000 address of the program memory as mapped in the kernel address space)
/// \return the start offset (where to jump to run the program, when in its address space).
///         (to get the kernel pointer to the prog start: \code base_addr + (void *)[return-value] \endcode
/// \return 0 on failure. (not a valid ELF, ...)
uint32_t load_elf(void *start_elf_addr, uint32_t elf_size, void *base_addr);



#endif /*__N_6721276161852313001_268001708__ELF_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

