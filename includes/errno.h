//
// file : errno.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/errno.h
//
// created by : Timothée Feuillet on linux.site
// date: 20/02/2015 14:35:18
//

#ifndef __N_60445472564392358_1640857941__ERRNO_H__
# define __N_60445472564392358_1640857941__ERRNO_H__

#define EUNKNOW   1         /* unknow error */
#define ENOSYS    2         /* unknow syscall */

#endif /*__N_60445472564392358_1640857941__ERRNO_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

