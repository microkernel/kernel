//
// file : serial.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/serial.h
//
// created by : Timothée Feuillet on linux.site
// date: 13/01/2015 19:49:58
//

#ifndef __N_16062751161466976722_679906136__SERIAL_H__
# define __N_16062751161466976722_679906136__SERIAL_H__

#include <kernel.h>
#include <types.h>
#include <asm_inoutb.h>


#define PORT 0x3F8


/* a serial console */
extern struct console serial_console;

inline static void serial_outb(uint8_t data)
{
  while ((inb(PORT + 5) & 0x20) == 0);
  outb(PORT, data);
}

// write something to the serial port
inline static void serial_out(const void *data, size_t size)
{
  for (size_t i = 0; i < size; ++i)
    serial_outb(*(uint8_t *)(data + i));
}

// write a C string
inline static void serial_out_cstr(const char *data)
{
  for (size_t i = 0; data[i]; ++i)
    serial_outb(*(uint8_t *)(data + i));
}

// initialize the serial port
void serial_init();

#endif /*__N_16062751161466976722_679906136__SERIAL_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

