//
// file : asm_inoutb.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/asm_inoutb.h
//
// created by : Timothée Feuillet on linux.site
// date: 05/01/2015 12:16:28

#ifndef __N_1497941143453216488_2052510603__ASM_INOUTB_H__
# define __N_1497941143453216488_2052510603__ASM_INOUTB_H__

#include "types.h"

/* in* and out* instruction for C */
inline static uint8_t inb (uint16_t port)
{
    uint8_t ret;
    __asm__ __volatile__ ("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

inline static void outb (uint16_t port, uint8_t data)
{
    __asm__ __volatile__ ("outb %1, %0" : : "dN" (port), "a" (data));
}

inline static uint16_t inw (uint16_t port)
{
    uint16_t ret;
    __asm__ __volatile__ ("inw %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

inline static void outw (uint16_t port, uint16_t data)
{
    __asm__ __volatile__ ("outw %1, %0" : : "dN" (port), "a" (data));
}

inline static uint32_t inl (uint16_t port)
{
    uint32_t ret;
    __asm__ __volatile__ ("inl %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

inline static void outl (uint16_t port, uint32_t data)
{
    __asm__ __volatile__ ("outl %1, %0" : : "dN" (port), "a" (data));
}

/* delay: write the 0x80 port (POST). This introduce a ~us delay due to the IO wait */
inline static void dummy_delay()
{
  const uint16_t port = 0x80; /* POST port */
  __asm__ __volatile__ ("outb %%al, %0" : : "dN" (port));
}

#endif /*__N_1497941143453216488_2052510603__ASM_INOUTB_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

