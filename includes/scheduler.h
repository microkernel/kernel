#ifndef __SCHEDULER_H__
# define	__SCHEDULER_H__

# include	"gdt.h"

# define	TS_RUNNING		0
# define	TS_RUNABLE		1
# define	TS_STOPPED		2

// typedef struct task_t {
//         struct tss_t tss;
//         unsigned long long tss_entry;
//         unsigned long long ldt[2];
//         unsigned long long ldt_entry;
//         int state;
//         int priority;
//         struct task_t *next;
// } task_t;

typedef struct task_t {
    struct tss_t		tss;
    uint32_t			state;
    uint32_t			priority;
    page_directory_t	*page_directory;
    struct task_t		*next;
} task_t;

// typedef struct process_t {
//     regs_t				regs;
//     page_directory_t	*pg_dir;
//     process_t			*next;
// } process_t;

#endif /* __SCHEDULER_H__ */
