#ifndef __GDT_H__
# define __GDT_H__

# include "types.h"

extern uint64_t		_gdt;
extern uint32_t		_gdt_ptr;

#define TSS_TYPE	(0x89)

/* #define BUILD_SEG_DESC(base_addr, limit, segtype)      \
//     ((struct segment_descriptor) {                     \  
//         .limit_15_0 = (limit) & 0xffff,                \  
//         .limit_19_16 = ((limit) >> 16) & 0xf,          \  
//         .base_addr_15_0 = (base_addr) & 0xffff,        \  
//         .base_addr_23_16 = ((base_addr) >> 16) & 0xff, \  
//         .base_addr_31_24 = ((base_addr) >> 24) & 0xff, \  
//         .u = 0, .x = 0, .d = 1, .g = 0,                \  
//         .type = (segtype)                              \  
//     })  
*/
/*
** Structures
*/
// typedef struct segment_descriptor_t {  
//     /* First 16 bits in the segment limiter. */  
//     uint16_t limit_low;
//     /* First 16 bits in the base address. */  
//     uint16_t base_low;
//     /* Bits 16-23 in the base address. */  
//     uint8_t base_middle;
//     /* Since all descriptors do not have exactly the same structure (e.g. a TSS 
//        descriptor has a slightly different structure from an LDT descriptor), 
//        we group several fields into the "type" member. */  
//     uint8_t type;
//     /* Bits 16-19 in the segment limiter. */  
//     uint8_t limit_middle:4;  
//     /* User (OS) defined bit. */  
//     uint8_t u:1;
//     /* Reserved by Intel. */  
//     uint8_t x:1;
//     /*  Indicates how the instructions (80386 and up) access register and memory 
//         data in protected mode. When D=0, instructions are 16-bit instructions, 
//         with 16-bit offsets and 16-bit registers. Stacks are assumed 16-bit wide 
//         and SP is used. When D=1, 32-bits are assumed. */  
//     uint8_t d:1;
//     /* When G=0, segments can be 1 byte to 1MB in length. 
//        When G=1, segments can be 4KB to 4GB in length. */  
//     uint8_t g:1;
//     /* The last 24-31 bits in the base address. */  
//     uint8_t base_high;
// } __attribute__((__packed__)) segment_descriptor_t;

typedef struct tss_t {
    uint32_t back_link; /* reserved, link */
    uint32_t esp0, ss0;
    uint32_t esp1, ss1;
    uint32_t esp2, ss2;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax,ecx,edx,ebx;
    uint32_t esp, ebp;
    uint32_t esi, edi;
    uint32_t es,cs,ss,ds,fs,gs;
    uint32_t ldt;
    uint32_t trace_bitmap; /* iopb, reserved */
} tss_t;

typedef struct gdt_entry_t {
    uint16_t	limit_low;
    uint16_t	base_low;
    uint8_t		base_middle;
    uint8_t		access;
    uint8_t		granularity;
    uint8_t		base_high;
} __attribute__((packed)) gdt_entry_t;

/*
** limit: The max bytes taken up by the GDT minus 1.
*/
typedef struct gdt_t {
    uint16_t limit;
    uint32_t base;
} __attribute__((packed)) gdt_t;

/*
** Prototypes
*/
extern void gdt_flush(uint32_t);
// void gdt_set_gate(int num, unsigned long base, unsigned long limit, unsigned char access, unsigned char gran);
void init_gdt(void *stack_ptr);

#endif /* __GDT_H__ */
