//
// file : syscall.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/userland/syscall.h
//
// created by : Timothée Feuillet on linux.site
// date: 19/02/2015 12:25:13
//

#ifndef __N_19454292251797762510_523555537__SYSCALL_H__
# define __N_19454292251797762510_523555537__SYSCALL_H__

#include "../syscall_table.h"
#include "../errno.h"
#include "asm_syscall.h"

static inline void sys_exit()
{
  _syscall_0(SYS_EXIT);
}

static inline void sys_panic()
{
  _syscall_0(SYS_PANIC);
}

static inline int sys_print(const char *str, uint32_t len)
{
  return _syscall_2(SYS_PRINT, (uint32_t)str, len);
}


#endif /*__N_19454292251797762510_523555537__SYSCALL_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 


