//
// file : asm_syscall.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/userland/asm_syscall.h
//
// created by : Timothée Feuillet on linux.site
// date: 19/02/2015 12:25:36
//

#ifndef __N_16680124641303836568_961927548__ASM_SYSCALL_H__
# define __N_16680124641303836568_961927548__ASM_SYSCALL_H__
// #include <unistd.h>

#include "../types.h"
#include "../syscall_table.h"

static inline int _syscall_0(uint32_t sysno)
{
  int ret;
  __asm__ volatile ("int $0x80;"
                    : "=a" (ret)
                    : "a" (sysno));
  return ret;
}

static inline int _syscall_1(uint32_t sysno, uint32_t a1)
{
  int ret;
  __asm__ volatile ("int $0x80;"
                    : "=a" (ret)
                    : "a" (sysno), "b" (a1));
  return ret;
}

static inline int _syscall_2(uint32_t sysno, uint32_t a1, uint32_t a2)
{
  int ret;
  __asm__ volatile ("int $0x80;"
                    : "=a" (ret)
                    : "a" (sysno), "b" (a1), "c" (a2));
  return ret;
}

static inline int _syscall_3(uint32_t sysno, uint32_t a1, uint32_t a2, uint32_t a3)
{
  int ret;
  __asm__ volatile ("int $0x80;"
                    : "=a" (ret)
                    : "a" (sysno), "b" (a1), "c" (a2), "d" (a3));
  return ret;
}

static inline int _syscall_4(uint32_t sysno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4)
{
  int ret;
  __asm__ volatile ("int $0x80;"
                    : "=a" (ret)
                    : "a" (sysno), "b" (a1), "c" (a2), "d" (a3), "S" (a4));
  return ret;
}

static inline int _syscall_5(uint32_t sysno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
  int ret;
  __asm__ volatile ("int $0x80;"
                    : "=a" (ret)
                    : "a" (sysno), "b" (a1), "c" (a2), "d" (a3), "S" (a4), "D" (a5));
  return ret;
}

#endif /*__N_16680124641303836568_961927548__ASM_SYSCALL_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 


