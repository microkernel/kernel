#ifndef __PAGING_H__
# define __PAGING_H__

# include	"types.h"

# define	INDEX_FROM_BIT(a)				(a / (8 * 4))
# define	OFFSET_FROM_BIT(a)				(a % (8 * 4))
# define	PAGE_SIZE						(4096)
# define	KERNEL_PHYSICAL_ADDRESS			(0x100000)
# define	KERNEL_VIRTUAL_ADDRESS			(0xC0000000)
# define	KERNEL_VIRTUAL_ADDRESS_END		(0xC0400000)
# define	MEM_START_PAGE					(0x0400000)
# define	MEM_END_PAGE					(KERNEL_VIRTUAL_ADDRESS)
# define	ADDRESS_TO_FRAME(x)				(((uint32_t) x) >> 12)
# define	FRAME_TO_ADDRESS(x)				(x << 12)
# define	ADDRESS_TO_PAGEDIR_INDEX(x)		((x >> 22) & 0x3FF)
# define	ADDRESS_TO_PAGETAB_INDEX(x)		((x >> 12) & 0x3FF)

# define invlpg(addr)   __asm__ __volatile__("invlpg (%0)" : : "r" (addr) : "memory");

typedef struct page_t
{
    uint32_t present	: 1;   // Page present in memory
    uint32_t rw			: 1;   // Read-only if clear, readwrite if set
    uint32_t user		: 1;   // Supervisor level only if clear
    uint32_t accessed	: 1;   // Has the page been accessed since last refresh?
    uint32_t dirty		: 1;   // Has the page been written to since last refresh?
    uint32_t unused		: 7;   // Amalgamation of unused and reserved bits
    uint32_t frame		: 20;  // Frame address (shifted right 12 bits)
} page_t;

typedef struct page_table_t
{
    page_t pages[1024];
} page_table_t;

typedef struct page_directory_t
{
    uint32_t present			: 1;   // Page present in memory
    uint32_t rw					: 1;   // Read-only if clear, readwrite if set
    uint32_t user				: 1;   // Supervisor level only if clear
    uint32_t write_through		: 1;   // Caching Strategy write through/write back
    uint32_t cache_disabled		: 1;   // Cache disabled
    uint32_t accessed			: 1;
    uint32_t reserved			: 1;
    uint32_t page_size			: 1;   // 0 for 4KB
    uint32_t ignored			: 1;   // Not used
    uint32_t avail				: 3;   // Amalgamation of unused and reserved bits
    uint32_t frame				: 20;  // Frame address (shifted right 12 bits)
} __attribute__((__packed__)) page_directory_t;

/*
** Prototypes
*/
void	initialise_paging();
void	switch_page_directory(page_directory_t *new_page);
page_t	*get_page(uint32_t address, int make, page_directory_t *dir);

void	*get_virtual_address(void *addr);

int8_t get_page_from_virtual_address(page_t **page, uint32_t addr);

void map_kernel(page_directory_t *page_directory,
                page_table_t *page_table);

// return 0: already present
// return 1: mapped
// return -1: fuck your code.
int8_t map_page_at_virtual_address(page_t **page, addr_t vaddr, addr_t paddr);

// map the physical page of orig_vaddr to dest_vaddr
// NOTE: it does not unmap orig_vaddr
int8_t remap_page_at_virtual_address(addr_t orig_vaddr, addr_t dest_vaddr);

// like remap_page_at_virtual_address but unmap
int8_t movemap_page_at_virtual_address(addr_t orig_vaddr, addr_t dest_vaddr);

// return 0: already unmapped
// return 1: unmapped
int8_t unmap_page_at_virtual_address(addr_t vaddr);



/*
** ASM
*/
void	_enable_paging();
void	load_page_directory(page_directory_t *);

#endif /* __PAGING_H__ */
