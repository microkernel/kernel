//
// file : syscall.h (2)
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/syscall.h
//
// created by : Timothée Feuillet on linux.site
// date: 20/02/2015 13:53:23
//

#ifndef __N_17581409871450681729_943044785__SYSCALL_H__2___
# define __N_17581409871450681729_943044785__SYSCALL_H__2___

#include "syscall_table.h"
#include "interrupt.h"

// the 5 last uint32_t parameters are (possible) parameters received by the syscall.
// (NOTE: you're allowed to cast the function pointer).
typedef int (* syscall_handler)(struct interrupt_parameters *, uint32_t , uint32_t , uint32_t , uint32_t, uint32_t);

// set syshandler to 0 if you want to remove the handler
void syscall_set_handler(uint32_t sysno, syscall_handler syshandler);

// should be called only one, at initialization.
void _init_syscalls();

#endif /*__N_17581409871450681729_943044785__SYSCALL_H__2___*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

