#ifndef __MEM_H
#define __MEM_H

#include "types.h"

void *memcpy(void *dest, const void *src, int count);
void *memset(void *dest, uint8_t val, int count);

#endif
