//
// file : types.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/types.h
//
// created by : Timothée Feuillet on linux.site
// date: 05/01/2015 12:18:13
//

#ifndef __N_1062307434830591440_778671569__TYPES_H__
# define __N_1062307434830591440_778671569__TYPES_H__

/* some lovely type definition for our plateform ! */
typedef unsigned char uint8_t;
typedef char int8_t;
typedef unsigned short uint16_t;
typedef short int16_t;
typedef unsigned int uint32_t;
typedef int int32_t;

typedef long long int int64_t;
typedef unsigned long long int uint64_t;

typedef void * ptr_t;
typedef long addr_t;
typedef unsigned long size_t;

typedef int bool;

#define true 1
#define false 0

#endif /*__N_1062307434830591440_778671569__TYPES_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

