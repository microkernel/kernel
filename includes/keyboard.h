//
// file : keyboard.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/keyboard.h
//
// created by : Timothée Feuillet on linux.site
// date: 20/02/2015 21:06:05
//
#ifndef __N_3520355041233507802_1177935935__KEYBOARD_H__
# define __N_3520355041233507802_1177935935__KEYBOARD_H__

#include "types.h"

enum KEYBOARD_MODIFIER_KEYS
{
  KMK_RELEASED          = 0x1000,
  KMK_SHIFT             = 0x0100,
  KMK_CTRL              = 0x0200,
  KMK_ALT               = 0x0400,

  KMK_CAPS_LOCK         = 0x9b,
  KMK_NUM_LOCK          = 0x9c,
  KMK_SCR_LOCK          = 0x9d,

  KMK_ESCAPE            = 27,
  KMK_DELETE            = 127,

  /* Function keys. */

  KMK_F1               = 0x81,
  KMK_F2               = 0x82,
  KMK_F3               = 0x83,
  KMK_F4               = 0x84,
  KMK_F5               = 0x85,
  KMK_F6               = 0x86,
  KMK_F7               = 0x87,
  KMK_F8               = 0x88,
  KMK_F9               = 0x89,
  KMK_F10              = 0x8a,
  KMK_F11              = 0x8b,
  KMK_F12              = 0x8c,

  /* Numeric keypad. */

  KMK_HOME             = 0x8d,
  KMK_END              = 0x8e,
  KMK_UP               = 0x8f,
  KMK_DOWN             = 0x90,
  KMK_LEFT             = 0x91,
  KMK_RIGHT            = 0x92,
  KMK_PGUP             = 0x93,
  KMK_PGDN             = 0x94,
  KMK_CENTER           = 0x95,
  KMK_INSERT           = 0x96
};

uint16_t kbd_get_char_code(uint8_t stroke, int shift_key);

#endif /*__N_3520355041233507802_1177935935__KEYBOARD_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

