//
// file : stdargs.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/stdargs.h
//
// created by : Timothée Feuillet on linux.site
// date: 13/01/2015 15:29:07
//

#ifndef __N_6768711201451803907_910473272__STDARGS_H__
# define __N_6768711201451803907_910473272__STDARGS_H__

typedef __builtin_va_list va_list;

#define va_start(v,l)   __builtin_va_start(v,l)
#define va_end(v)       __builtin_va_end(v)
#define va_arg(v,l)     __builtin_va_arg(v,l)
#define va_copy(d,s)    __builtin_va_copy(d,s)

#endif /*__N_6768711201451803907_910473272__STDARGS_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

