//
// file : syscall_table.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/syscall_table.h
//
// created by : Timothée Feuillet on linux.site
// date: 19/02/2015 12:48:07
//

#ifndef __N_14186983921849383019_373934723__SYSCALL_TABLE_H__
# define __N_14186983921849383019_373934723__SYSCALL_TABLE_H__


#define SYSCALL_TABLE_SIZE    255
#define SYSCALL_INTERRUPT     0x80


#define SYS_EXIT              4
#define SYS_PRINT             5

// for TEST purposes
#define SYS_PANIC             254


#endif /*__N_14186983921849383019_373934723__SYSCALL_TABLE_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

