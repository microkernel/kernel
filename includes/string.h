//
// file : string.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/string.h
//
// created by : Timothée Feuillet on linux.site
// date: 13/01/2015 14:48:10
//

#ifndef __N_10081252851873280069_372288540__STRING_H__
# define __N_10081252851873280069_372288540__STRING_H__

#include <types.h>

size_t strlen(const char *str);
size_t strnlen(const char *str, size_t maxlen);
size_t strlen_delim(const char *s, char delim);
size_t strlen_delims(const char *s, const char *delims);


char  *strcpy(char *s1, const char *s2);
char  *strncpy(char *s1, const char *s2, size_t n);
char  *strncat(char *s1, const char *s2, size_t n);
char  *strcat(char *s1, const char *s2);
int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, size_t n);

// string to int conversions
size_t atoul_base(const char *str, const char *base);
long atol_base(const char *str, const char *base);
int isdigit_base(char c, const char *base);

// int to string conversions
size_t ultoa_base(size_t number, const char *base, char *str, size_t maxsize);
size_t ltoa_base(long number, const char *base, char *str, size_t maxsize);

#define BASE_16_DIGITS        "0123456789ABCDEF"
#define BASE_16_DIGITS_LOWER  "0123456789abcdef"
#define BASE_10_DIGITS        "0123456789"
#define BASE_8_DIGITS         "01234567"

// // helpers for ato.?l_base(...) functions
// base 10
inline static size_t atoul(const char *str)
{
  return atoul_base(str, BASE_10_DIGITS);
}
inline static long atol(const char *str)
{
  return atol_base(str, BASE_10_DIGITS);
}

// base 16
inline static size_t atoul_hex(const char *str)
{
  return atoul_base(str, BASE_16_DIGITS);
}
inline static long atol_hex(const char *str)
{
  return atol_base(str, BASE_16_DIGITS);
}

// base 8
inline static size_t atoul_oct(const char *str)
{
  return atoul_base(str, BASE_8_DIGITS);
}
inline static long atol_oct(const char *str)
{
  return atol_base(str, BASE_8_DIGITS);
}

// // helpers for .?ltoa_base(...) functions
// base 10
inline static size_t ultoa(size_t number, char *str, size_t maxsize)
{
  return ultoa_base(number, BASE_10_DIGITS, str, maxsize);
}
inline static size_t ltoa(size_t number, char *str, size_t maxsize)
{
  return ltoa_base(number, BASE_10_DIGITS, str, maxsize);
}

// base 16
inline static size_t ultoa_hex(size_t number, char *str, size_t maxsize)
{
  return ultoa_base(number, BASE_16_DIGITS, str, maxsize);
}
inline static size_t ltoa_hex(size_t number, char *str, size_t maxsize)
{
  return ltoa_base(number, BASE_16_DIGITS, str, maxsize);
}

// base 8
inline static size_t ultoa_oct(size_t number, char *str, size_t maxsize)
{
  return ultoa_base(number, BASE_8_DIGITS, str, maxsize);
}
inline static size_t ltoa_oct(size_t number, char *str, size_t maxsize)
{
  return ltoa_base(number, BASE_8_DIGITS, str, maxsize);
}

#endif /*__N_10081252851873280069_372288540__STRING_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

