#ifndef	__MALLOC_H__
# define	__MALLOC_H__

# include "types.h"

/*
** Prototypes
*/
void *malloc(size_t);
void init_malloc(uint32_t);

#endif /* __MALLOC_H__ */