#ifndef __KMALLOC_H__
# define __KMALLOC_H__

# include "types.h"

/*
** Prototypes
*/
void *kmap_area(size_t page_count);
void *kmap_area_to(void *vaddr, size_t page_count);

// free both the virtual pages AND the physical ones
void kunmap_area(void *vaddr, size_t page_count);

// like kfree, but don't put the physical pages into the free list.
// WARNING: beware of the memory leaks !!!
void kunmap_area_soft(void *vaddr, size_t page_count);

void *kmap_linear(size_t page_count);

#endif /* __KMALLOC_H__ */