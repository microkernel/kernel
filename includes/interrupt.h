//
// file : interrupt.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/interrupt.h
//
// created by : Timothée Feuillet on linux.site
// date: 12/01/2015 18:05:14
//

#ifndef __N_10067104891856706471_1176789497__INTERRUPT_H__
# define __N_10067104891856706471_1176789497__INTERRUPT_H__

#include "types.h"
#include "asm_interrupt.h"
#include "irq.h"

#define IDT_TASK_GATE_TYPE      0x05
#define IDT_INTERUPT_GATE_TYPE  0x0e
#define IDT_TRAP_GATE_TYPE      0x0f

#define IRQ_INTERRUPT_START_NUMBER      0x20
#define IRQ_INTERRUPT_END_NUMBER        0x30

// what an interrupt handler receive as parameter:
struct interrupt_parameters
{
  // register save
  uint32_t ss, gs, fs, es, ds;
  uint32_t interrupt_number;
  uint32_t edi, esi, ebp, esp, ebx, edx, ecx, _;
  uint32_t eax;

  // infos
  uint32_t error_code;

  // CPU pushed this
  uint32_t eip, cs, eflags, user_esp, user_ss;   // user_esp and user_ss **MAY NOT BE PRESENT**
};

// the type of a handler
typedef void (*interrupt_handler_t)(struct interrupt_parameters *);

// a generic way to set interrupts handlers
void idt_set_gate(uint8_t number, uint8_t privilege, interrupt_handler_t callback);

static inline void irq_set_handler(uint8_t irq_number, interrupt_handler_t callback)
{
  idt_set_gate(IRQ_INTERRUPT_START_NUMBER + irq_number, 0, callback);
  irq_activate(irq_number);
}

// install and init the idt
void _idt_install();
void _idt_setup_default_routines();

#endif /*__N_10067104891856706471_1176789497__INTERRUPT_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

