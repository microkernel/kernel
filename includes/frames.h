#ifndef __FRAMES_H__
# define __FRAMES_H__

# include "paging.h"
# include "types.h"

/*
** Prototypes
*/

void init_frames();
uint32_t  align_address(void *address);

// return 0 when the user can screw himself
uint32_t frame_get_first_free();
// {
//   return 0;
// }

// remove an used frame
void frame_push_to_free_list(uint32_t frame);

#endif /* __FRAMES_H__ */
