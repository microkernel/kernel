#ifndef __PRINTK_H__
# define __PRINTK_H__

#include "types.h"
#include "stdargs.h"

/*
** console (allow to switch console, and remove the vga code from printk)
*/
typedef uint16_t coord_t;
struct console
{
  coord_t cursor_x;
  coord_t cursor_y;

  coord_t size_x;
  coord_t size_y;

  uint16_t mode;

  const int handle_special_chars; // handles '\n' and...

  /* print a single char at a cursor_{x,y} coordinates. cursor_{x,y} are left unchanged */
  void (*putchar)(struct console *self, char c);
  /* clear the console */
  void (*clear)(struct console *self);

  /* scroll the buffer of 'line' lines */
  void (*scroll_buffer)(struct console *self, unsigned short line);

  void (*init)(struct console *self);
};

/* the default vga console */
extern struct console vga_console;

/*
** Prototypes
*/
/* set the console used by the kernel (default: vga console) */
void set_kernel_console(struct console *cons);
struct console *get_kernel_console();

void console_clear();
void console_clear_console(struct console *cons);
void printk_str(const char *s);
void printk_str_console(struct console *cons, const char *s);

// accepted format:
//   %%:      print %
//   %s:      print a string
//   %x/%X:   print a unsigned number in hex
//   %o:      print a unsigned number in octal
//   %u/%U:   print a unsigned number in base 10
//   %b:      print a unsigned number in base 2
//   %i:      print a signed number in base 10
//
//   %M:      set console mode
//   %D:      set console mode to DEFAULT_MODE
void printk(const char *fmt, ...);
void va_printk(const char *fmt, va_list lst);

void printk_console(struct console *cons, const char *fmt, ...);
void va_printk_console(struct console *cons, const char *fmt, va_list lst);

void putchar(char c);
void putchar_console(struct console *cons, char c);

/* please, do not call */
void _init_printk();

enum KERNEL_CONSOLE_COLORS
{
  // valid for background and foreground
  KCC_BLACK = 0,
  KCC_BLUE = 1,
  KCC_GREEN = 2,
  KCC_CYAN = 3,
  KCC_RED = 4,
  KCC_MAGENTA = 5,
  KCC_BROWN = 6,
  KCC_LIGHT_GRAY = 7,

  // background only (or'd with the background color)
  // (may not works)
  // (doesn't works on QEMU)
  KCC_BLINK = 8,

  // foreground only (or both, in QEMU -- at least)
  KCC_DARK_GRAY = 8,
  KCC_LIGHT_BLUE = 9,
  KCC_LIGHT_GREEN = 10,
  KCC_LIGHT_CYAN = 11,
  KCC_LIGHT_RED = 12,
  KCC_LIGHT_MAGENTA = 13,
  KCC_YELLOW = 14,
  KCC_WHITE = 15,
};

#define KCC_TO_MODE(background, foreground)       (((((int)background) & 0xF) << 4) | (((int)foreground) & 0xF))
#define DEFAULT_MODE                              KCC_TO_MODE(KCC_BLACK, KCC_LIGHT_GRAY)

#endif
