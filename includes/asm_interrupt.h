//
// file : asm_interrupt.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/asm_interrupt.h
//
// created by : Timothée Feuillet on linux.site
// date: 12/01/2015 13:29:12
//

#ifndef __N_2009019931133903945_34420785__ASM_INTERRUPT_H__
# define __N_2009019931133903945_34420785__ASM_INTERRUPT_H__

#include "types.h"

// describe an IDT
struct idt_descr
{
  uint16_t size;
  uint32_t addr;
} __attribute__((packed));

/// \brief set the IDT from a base address (\p addr) and a \p size
inline static void set_idt(void *addr, uint16_t size)
{
  struct idt_descr idtr;
  idtr.size = size;
  idtr.addr = (uint32_t) addr;
  asm("lidt %0" : : "m"(idtr) : "memory");
}

/// \brief fill \p idtr with infos about the IDT
inline static void get_idt(struct idt_descr *idtr)
{
  asm volatile("sidt (%0)" : "=p"(idtr) : : "memory");
}

// disable interrupts (wrapper around cli)
// NOTE: this shouldn't be used directly, except for special cases.
//       Please use the INTERRUPT_CRITICAL_SECTION / INTERRUPT_CRITICAL_SECTION_END
inline static void _disable_interrupts()
{
  asm("cli" : :);
}

// enable interrupts (wrapper around sti)
// NOTE: this shouldn't be used directly, except for special cases.
//       Please use the INTERRUPT_CRITICAL_SECTION / INTERRUPT_CRITICAL_SECTION_END
inline static void _enable_interrupts()
{
  asm("sti" : :);
}

// those macros (stolen from the Simplix Kernel) allow disabling and restoring interrupts
// and remember if their were enabled or disabled before
#define __disable_interrupts(eflags) asm("pushf; pop %0; cli;" : "=g" ((eflags)))
#define __restore_interrupts(eflags) asm("push %0; popf;" :: "g" ((eflags)))

// start a section where interrupts MUST be disabled
#define INTERRUPT_CRITICAL_SECTION      {uint32_t _k_eflags__;__disable_interrupts(_k_eflags__);{
#define INTERRUPT_CRITICAL_SECTION_END  }__restore_interrupts(_k_eflags__);}

#endif /*__N_2009019931133903945_34420785__ASM_INTERRUPT_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

