//
// file : irq.h
// in : file:///home/tim/labs-tp/t5-2/kernel/includes/irq.h
//
// created by : Timothée Feuillet on linux.site
// date: 17/01/2015 11:45:27
//

#ifndef __N_31158471126458522_1425695285__IRQ_H__
# define __N_31158471126458522_1425695285__IRQ_H__

#include "asm_inoutb.h"
#include "types.h"

#define PIC1    0x20    /* IO base address for master PIC */
#define PIC2    0xA0    /* IO base address for slave PIC */
#define PIC1_COMMAND  PIC1
#define PIC1_DATA (PIC1+1)
#define PIC2_COMMAND  PIC2
#define PIC2_DATA (PIC2+1)


// PIC things
uint16_t pic_get_irr(void); /// \brief return the combined IRR
uint16_t pic_get_isr(void); /// \brief return the combined ISR

// IRQs
void irq_activate(uint8_t line);
void irq_deactivate(uint8_t line);

inline static void pic_send_eoi(uint8_t irq)
{
# define PIC_EOI    0x20    /* end of interrupt CC */
  if (irq >= 8)
    outb(PIC2_COMMAND, PIC_EOI);
  outb(PIC1_COMMAND, PIC_EOI);
}

/// test if the irq is a spurious one
inline static bool irq_is_spurious(uint8_t current_irq)
{
  const uint16_t sr = 1 << current_irq;
  return !(sr && pic_get_isr());
}

/// \brief send eoi if the interrupt is not a spurious one, else return false.
inline static bool irq_handle_spurious(uint8_t current_irq)
{
  if (!irq_is_spurious(current_irq))
  {
    pic_send_eoi(current_irq);
    return true;
  }
  return false;
}


// p is a struct interrupt_parameters *
#define GET_IRQ_NUMBER(p)           (p->interrupt_number - IRQ_INTERRUPT_START_NUMBER)

// p is a struct interrupt_parameters *
#define IRQ_INTERRUPT_PROLOG(p)     do {if (!irq_handle_spurious(GET_IRQ_NUMBER(p))) return; } while (0)

#endif /*__N_31158471126458522_1425695285__IRQ_H__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 

