##
## Makefile for MicroKernel
##

DEBUG?=	yes
SHELL=	/bin/bash
ARCH=	$(shell uname -m)
TYPE=	$(shell uname -s)
NAME=	kernel.bin

CC?=	gcc
ASM?=	nasm -f elf
LD?=	ld
RM?=	rm
OBJCOPY?=objcopy

# test if makedepend is present, else replace it with : (a noop command)
DEPEND=	$(shell hash makedepend 2>/dev/null && echo makedepend || echo :)

KFLAGS= -nostdinc -pie \
 	-fno-builtin \
 	-fno-stack-protector \
 	-mno-mmx \
 	-mno-3dnow \
 	-mno-sse
# 	-target=i386-none-eabi \


CFLAGS=		-std=gnu99 -W -Wall -Wextra
CFLAGS+=	-m32

BUILD?=		build/
HDRDIR=		includes/
LIBDIR=		lib/

CFLAGS+=	-I ./$(HDRDIR)
CFLAGS+=	-Wl,-rpath -Wl,`pwd`/$(LIBDIR)

PFLAGS=		-nostdlib -nostartfiles -nodefaultlibs -pie -Wl,-m -Wl,elf_i386 
#-Wl,-s

ASM_TARGET=	elf
ASMFLAGS=	

ARCH_LDFLAGS=	-m elf_i386
OBJCOPY_FLAGS=	-I binary -O elf32-i386 -B i386 --set-section-flags .data=load,alloc
#--rename-section .data=.data,load,alloc -vvvv

LDFLAGS=	-L ./$(LIBDIR) $(ARCH_LDFLAGS) -T link.ld -static

BUILD_ASM=		${BUILD}/asm/
BUILD_C=		${BUILD}/c/
BUILD_CXX=		${BUILD}/cxx/
BUILD_ELF=		${BUILD}/elf/

# c sources
C_SRC=		./src/main.c \
		./src/vga.c \
		./src/mem.c \
		./src/string.c \
		./src/gdt.c \
		./src/idt.c \
		./src/pic.c \
		./src/panic.c \
		./src/idt_default_routines.c \
		./src/frames.c \
		./src/paging.c \
		./src/paging/kmalloc.c \
		./src/printk.c \
		./src/elf.c \
		./src/keyboard.c \
		./src/malloc.c \
		./src/scheduler.c \
		./src/serial.c \
		./src/syscall.c \

# embedded C sources (will be linked and embedded in the final executable)
E_C_SRC=	./prog/main_1.c \

# what will compose the runtime of progs (could be either C, C++ or ASM files)
# (this will create a static library (libruntime.a) that will be linked to every prog in E_C_SRC)
EL_AS_SRC=	./prog/runtime/start.S
EL_C_SRC=	
EL_CX_SRC=	
EL_OUTPUT=	${BUILD}/libruntime.a

# c++ sources, if needed
CX_SRC=		

# assembly sources
AS_SRC=		./boot.S \
		./src/gdt.S \
		./src/idt.S \
		./src/paging.S \
		#./src/paging/paging.S \

ifeq ($(DEBUG),yes)
  CFLAGS+=	-g -Og
  ASMFLAGS +=	-g
else
  CFLAGS+=	-O2
endif

# sources to objects
_C_SRC= $(C_SRC:%.c=${BUILD_C}/%.o)
__E_C_SRC= $(E_C_SRC:%.c=${BUILD_ELF}/%.elf)
_E_C_SRC= $(__E_C_SRC:%.elf=%.o)
_CX_SRC=$(CX_SRC:%.cpp=${BUILD_CXX}/%.o)
_AS_SRC=$(AS_SRC:%.S=${BUILD_ASM}/%.o)
_ELF_SRC=$(ELF_SRC:%.elf=${BUILD_ELF}/%.o)
_SRC= $(_C_SRC) $(_CX_SRC) $(_AS_SRC) $(_E_C_SRC)
OBJ= $(_SRC)

_EL_C_SRC= $(EL_C_SRC:%.c=${BUILD_C}/%.o)
_EL_CX_SRC=$(EL_CX_SRC:%.cpp=${BUILD_CXX}/%.o)
_EL_AS_SRC=$(EL_AS_SRC:%.S=${BUILD_ASM}/%.o)
_EL_SRC= $(_EL_C_SRC) $(_EL_CX_SRC) $(_EL_AS_SRC)
EL_OBJ= $(_EL_SRC)

# progress in percent
COUNT=	$(shell echo "$(OBJ)" | wc -w)
F_INC=	0

# main rule: all
all:
	@$(MAKE) "$(EL_OUTPUT)"
	@$(MAKE) "$(NAME)"


# rules to build things
${BUILD_C}/%.o: %.c
	@mkdir -p $(dir $@)
	@$(eval F_INC += "+1" )
	@echo -e "["`echo "(($(F_INC)) * 100) / $(COUNT)"|bc`"%]\tCC\t" $<
	@$(CC) -o $@ -c $< $(CFLAGS) $(KFLAGS)

${BUILD_CXX}/%.o: %.cpp
	@mkdir -p $(dir $@)
	@$(eval F_INC += "+1" )
	@echo -e "["`echo "(($(F_INC)) * 100) / $(COUNT)"|bc`"%]\tCXX\t" $<
	@$(CXX) -o $@ -c $< $(CPPFLAGS) $(KFLAGS)

${BUILD_ASM}/%.o: %.S
	@mkdir -p $(dir $@)
	@$(eval F_INC += "+1" )
	@echo -e "["`echo "(($(F_INC)) * 100) / $(COUNT)"|bc`"%]\tAS\t" $<
	@$(ASM) -f $(ASM_TARGET) -o $@ $< $(ASMFLAGS)

%.o: %.elf
	@mkdir -p $(dir $@)
	@echo -e "\tBLOB\t" $<
	@$(OBJCOPY) $(OBJCOPY_FLAGS) $< $@

${BUILD_ELF}/%.elf: %.c
	@mkdir -p $(dir $@)
	@$(eval F_INC += "+1" )
	@echo -e "\tCCLD\t" $<
	@$(CC) -o $@ $< $(CFLAGS) $(KFLAGS) $(PFLAGS) $(EL_OUTPUT)
	@cp $@ $@.bin

$(NAME): $(OBJ)
	@echo -e "\tLD\t $@ [ $(LD) $(LDFLAGS) -o $@ ]"
	@$(LD) $(LDFLAGS) -o $@ $^

# the runtime lib
$(EL_OUTPUT): $(EL_OBJ)
	@echo -e "\tAR\t $@"
	@ar rcs $@ $(EL_OBJ)
	@echo -e "\\tRANLIB\\t $@"
	@ranlib $@

# other rules
clean:
	@echo -e "\tCLEAN\t objects"
	@rm -f $(OBJ)
	@rm -f $(EL_OBJ)
	@echo -e "\tCLEAN\t backup files"
	@find . -name '*~' -type f -delete
	@find . -name "*\#" -delete

fclean: clean
	@rm -f $(EL_OUTPUT)
	@rm -f $(NAME)
	@echo -e "\tCLEAN\t binaries"

mrproper: $(NAME) clean

# allow make re -j5
re: fclean
	@$(MAKE) all


depend:
	@echo -e "\tDEPEND"
	@$(DEPEND) -w900 -- $(CFLAGS) $(CPPFLAGS) $(KFLAGS) -- $(C_SRC) $(CX_SRC)

.PHONY:	all clean fclean mrproper re depend

# keep the line below untouched, makedepend depends on it !
# DO NOT DELETE

./src/main.o: ./includes/kernel.h ./includes/macro.h ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/multiboot.h ./includes/asm_inoutb.h ./includes/gdt.h ./includes/interrupt.h ./includes/asm_interrupt.h ./includes/irq.h ./includes/paging.h ./includes/serial.h
./src/printk.o: ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/string.h ./includes/kernel.h ./includes/macro.h ./includes/multiboot.h
./src/vga.o: ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/asm_inoutb.h
./src/mem.o: ./includes/kernel.h ./includes/macro.h ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/multiboot.h
./src/string.o: ./includes/string.h ./includes/types.h
./src/gdt.o: ./includes/mem.h ./includes/types.h ./includes/gdt.h
./src/idt.o: ./includes/asm_interrupt.h ./includes/types.h ./includes/interrupt.h ./includes/irq.h ./includes/asm_inoutb.h ./includes/kernel.h ./includes/macro.h ./includes/printk.h ./includes/stdargs.h ./includes/multiboot.h
./src/pic.o: ./includes/types.h ./includes/asm_inoutb.h ./includes/irq.h
./src/panic.o: ./includes/kernel.h ./includes/macro.h ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/multiboot.h ./includes/asm_interrupt.h ./includes/asm_inoutb.h
./src/idt_default_routines.o: ./includes/kernel.h ./includes/macro.h ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/multiboot.h ./includes/interrupt.h ./includes/asm_interrupt.h ./includes/irq.h ./includes/asm_inoutb.h ./includes/serial.h
./src/serial.o: ./includes/serial.h ./includes/kernel.h ./includes/macro.h ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/multiboot.h ./includes/asm_inoutb.h ./includes/string.h
./src/paging.o: ./includes/printk.h ./includes/types.h ./includes/stdargs.h ./includes/interrupt.h ./includes/asm_interrupt.h ./includes/irq.h ./includes/asm_inoutb.h ./includes/paging.h
./src/elf.o: ./includes/elf.h ./includes/types.h
